## Endurance Marketplace

### Tech used

- Android, Kotlin
- DB: [CloudFirestore](https://console.firebase.google.com/project/mad-2020-8280b)
- SingIn: GoogleSignIn
- Storage: Firebase storage
- Backend support: Node.js for Cloud Functions to support the features of the app and push notifications
- Full text search: [Algolia](https://www.algolia.com/)

### Info about running the app

- In order for GoogleSignIn to work correctly after building the app, copy the SHA1 identifier and paste it into firebase project settings in firebase.

### Quirks 

- To add an image to User profile or Item use Long tap instead of single tap
- Shortcut to remove an item from interests. On My Interests page, swipe left on the cards to remove them
