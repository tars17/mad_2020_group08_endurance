package com.mad2020.group8.endurance.ui.rating_owner

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.data.User
import com.mad2020.group8.endurance.helper.Keyboard
import com.mad2020.group8.endurance.viewmodel.RatingOwnerViewModel
import kotlinx.android.synthetic.main.fragment_rating_owner.*


class RatingOwnerFragment: Fragment() {
    private lateinit var vm: RatingOwnerViewModel
    private lateinit var userid:String
    private lateinit var owner: User
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        vm = ViewModelProvider(this).get(RatingOwnerViewModel::class.java)
        return inflater.inflate(R.layout.fragment_rating_owner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var preCommentPeopleNum = 0
        var preScore  = 0.0
        var Score =0.0
        var sumScore=0.0
        var CommentPeopleNum = 0
        super.onViewCreated(view, savedInstanceState)
        lateinit var newUser: User
        vm.getOwner(arguments?.get("userID") as String).observe(viewLifecycleOwner, Observer{
            if(it.score!=0.0 && it.peoplenum!=0)
            {
                preCommentPeopleNum= it.peoplenum!!
                preScore = it.score!!
            }
            newUser=it
        })

        submitBtn.setOnClickListener {
            Keyboard.hide(requireActivity())
            //update Userprofile
            CommentPeopleNum = preCommentPeopleNum + 1
            sumScore=this.ratingOwnerBar.rating+(preScore*preCommentPeopleNum)
            Score = (sumScore)/(CommentPeopleNum)
            newUser.peoplenum=CommentPeopleNum
            newUser.score=Score
            if(!commentInput.text.isNullOrEmpty()) {
                newUser.commentList?.add(commentInput.text.toString())
            }
            vm.updateOwner(newUser)

            vm.updateupdateItemSoldedStatus(arguments?.get("itemID") as String)
            findNavController().navigate(R.id.action_nav_rating_owner_to_on_item_bought)

        }
    }
}