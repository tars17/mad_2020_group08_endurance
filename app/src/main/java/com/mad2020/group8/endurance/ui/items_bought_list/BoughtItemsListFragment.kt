package com.mad2020.group8.endurance.ui.items_bought_list

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.adapter.ItemAdapter
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.viewmodel.BoughtItemsListViewModel
import kotlinx.android.synthetic.main.fragment_bought_items_list.*

class BoughtItemsListFragment : Fragment() {
    private lateinit var adapter: ItemAdapter
    private lateinit var vm: BoughtItemsListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        vm = ViewModelProvider(this).get(BoughtItemsListViewModel::class.java)
        return inflater.inflate(R.layout.fragment_bought_items_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = ItemAdapter(emptyList(), false)

        adapter.onItemClick = { item, position ->  onItemClick(item, position) }
        boughtItemsListRecycleView.layoutManager = LinearLayoutManager(context)
        boughtItemsListRecycleView.adapter = adapter

        vm.getItems().observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                adapter.setItems(it.sortedByDescending { item -> item.createdAt })
                boughtItemsEmptyListCard.visibility = View.GONE
            } else {
                boughtItemsEmptyListCard.visibility = View.VISIBLE
            }
        })

    }

    private fun onItemClick(item: AdvertisementItem, position: Int) {
        findNavController().navigate(R.id.action_nav_bought_items_to_item_details_fragment, bundleOf("itemID" to item.id))
    }
}