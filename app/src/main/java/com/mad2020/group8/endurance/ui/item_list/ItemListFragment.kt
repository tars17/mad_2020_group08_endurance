package com.mad2020.group8.endurance.ui.item_list

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.adapter.ItemAdapter
import com.mad2020.group8.endurance.viewmodel.AdvertisementListViewModel
import kotlinx.android.synthetic.main.fragment_item_list.*

class ItemListFragment : Fragment() {
    private lateinit var vm: AdvertisementListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vm = ViewModelProvider(this).get(AdvertisementListViewModel::class.java)
        return inflater.inflate(R.layout.fragment_item_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = ItemAdapter(emptyList())
        adapter.onItemClick = { item, position ->  onItemClick(item, position) }
        adapter.onItemEditClick = { item, position ->  onEditItemClick(item, position) }

        itemListRecycleView.layoutManager = LinearLayoutManager(context)
        itemListRecycleView.adapter = adapter

        // main plus fab
        addItemFab.setOnClickListener {
            findNavController().navigate(R.id.action_nav_item_list_to_item_edit_fragment, bundleOf("id" to 0))
        }

        vm.getItems().observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                adapter.setItems(it.sortedByDescending { item -> item.createdAt })
                emptyListCard.visibility = View.GONE
            } else {
                emptyListCard.visibility = View.VISIBLE
            }
        })
    }

    private fun onItemClick(item: AdvertisementItem, position: Int) {
        findNavController().navigate(R.id.action_nav_item_list_to_item_details_fragment, bundleOf("itemID" to item.id))
    }

    private fun onEditItemClick(item: AdvertisementItem, position: Int) {
        findNavController().navigate(R.id.action_nav_item_list_to_item_edit_fragment, bundleOf("itemID" to item.id, "fromItemsList" to true))
    }
}
