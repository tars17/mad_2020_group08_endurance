package com.mad2020.group8.endurance.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.data.User
import com.squareup.picasso.Picasso

class UserAdapter (private var likedPeopleList : List<User>): RecyclerView.Adapter<UserAdapter.ViewHolder>() {
    var onPersonClick: ((person: User, position: Int) -> Unit)? = null

    override fun getItemCount() = likedPeopleList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.people_list_person, parent, false)

        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind( likedPeopleList[position])
    }


    inner class ViewHolder(v: View): RecyclerView.ViewHolder(v) {
        var personName = v.findViewById<TextView>(R.id.personNameTextView)
        var personEmail = v.findViewById<TextView>(R.id.personEmailTextView)
        var personPicture = v.findViewById<ImageView>(R.id.personPicture)

        init {
            itemView.setOnClickListener {
                onPersonClick?.invoke(likedPeopleList[adapterPosition], adapterPosition)
            }
        }


        fun bind (user: User) {
            personName.text = user.fullname
            personEmail.text = user.email

            if (!user.picture.isNullOrEmpty()) {
                Picasso.get().load(user.picture)
                    .placeholder(R.drawable.avatar_placeholder)
                    .error(R.drawable.avatar_placeholder)
                    .into(personPicture)
            }
        }
    }

    fun setUsers(likedPeopleList:List<User>) {
        val oldList = this.likedPeopleList
        val diffResult = DiffUtil.calculateDiff(UserDiffCallback(
            oldList,
            likedPeopleList
        ))

        this.likedPeopleList = likedPeopleList
        diffResult.dispatchUpdatesTo(this)
    }

    class UserDiffCallback(
        var oldList: List<User>,
        var newList: List<User>
    ): DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].equals(newList[newItemPosition])
        }

    }

}