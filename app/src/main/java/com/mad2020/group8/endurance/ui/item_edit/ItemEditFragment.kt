package com.mad2020.group8.endurance.ui.item_edit

import android.Manifest
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.mad2020.group8.endurance.MainActivity
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.data.User
import com.mad2020.group8.endurance.databinding.FragmentItemEditBinding
import com.mad2020.group8.endurance.helper.Auth
import com.mad2020.group8.endurance.helper.Category
import com.mad2020.group8.endurance.helper.Image
import com.mad2020.group8.endurance.helper.Keyboard
import com.mad2020.group8.endurance.map.LocationApplication
import com.mad2020.group8.endurance.viewmodel.EditAdvertisementViewModel
import com.mad2020.group8.endurance.viewmodel.EditAdvertisementViewModelFactory
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_item_edit.*
import java.io.File
import java.util.*

class ItemEditFragment : Fragment(), DatePickerDialog.OnDateSetListener,
    SellItemDialogFragment.SellItemDialogListener {
    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_IMAGE_GALLERY = 0
    private val PERMISSIONS_REQUEST_READ_STORAGE = 2
    private val PERMISSIONS_LOCATION = 3

    private var googleMap: GoogleMap? = null
    private lateinit var vm: EditAdvertisementViewModel
    private lateinit var vmFactory: EditAdvertisementViewModelFactory
    private var itemAddress = ""
    private var location = ""

    private var picturePath: String = ""
    private var storagePath: String = ""
    private var latLng = LatLng(39.937795, 116.387224)
    private lateinit var sellItemDialogFragment: SellItemDialogFragment


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)

        val itemID = arguments?.get("itemID") as String?
        if (itemID.isNullOrEmpty()) {
            (activity as MainActivity).supportActionBar?.title = "Add new item"
        } else {
            (activity as MainActivity).supportActionBar?.title = "Edit item"
        }

        vmFactory = EditAdvertisementViewModelFactory(
            itemID,
            arguments?.getBoolean("fromItemsList") as Boolean
        )
        vm = ViewModelProvider(this, vmFactory).get(EditAdvertisementViewModel::class.java)

        val binding: FragmentItemEditBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_item_edit, container, false)
        binding.vm = vm
        binding.lifecycleOwner = this

        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        expirationInput.setOnClickListener {
            this.showDatePicker()
        }

        val categories = Category.getCategories()
        categoriesSpinner.adapter = ArrayAdapter(
            context as Context,
            R.layout.support_simple_spinner_dropdown_item,
            categories
        )
        subcategoriesSpinner.adapter = ArrayAdapter(
            context as Context,
            R.layout.support_simple_spinner_dropdown_item,
            Category.getSubcategories(categories[0])
        )

        categoriesSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val category = categories[position]
                var oldSelectedSubCatIndex = -1
                if (subcategoriesSpinner.selectedItem.toString().isNotEmpty()) {
                    oldSelectedSubCatIndex = Category.getSubcategories(category)
                        .indexOfFirst { it == subcategoriesSpinner.selectedItem.toString() }
                }

                subcategoriesSpinner.adapter = ArrayAdapter(
                    context as Context,
                    R.layout.support_simple_spinner_dropdown_item,
                    Category.getSubcategories(category)
                )
                if (oldSelectedSubCatIndex != -1) {
                    subcategoriesSpinner.setSelection(oldSelectedSubCatIndex, true)
                }
            }
        }

        sellBtn.setOnClickListener {
            sellItem()
        }

        if (vm.getAdvertisementId().isNullOrEmpty()) {
            isBlockedSwitch.visibility = View.GONE
            sellBtn.visibility = View.GONE
        }




        val mapView: MapView = view.findViewById(R.id.mapView4)
        mapView.onCreate(savedInstanceState)
        mapView.isClickable = true

        vm.getAdvertisement().observe(viewLifecycleOwner, androidx.lifecycle.Observer {
//            titleInput.setText(it.title)
            descriptionInput.setText(it.description)
            locationTextView2.text = it.address
            priceInput.setText(it.price.toString())
            expirationInput.setText(it.expirationDate)

            isBlockedSwitch.isChecked = it.blocked

            val catIndex = categories.indexOfFirst { cat -> cat == it.category }
            if (catIndex != -1) {
                categoriesSpinner.setSelection(catIndex, true)
            }

            subcategoriesSpinner.adapter = ArrayAdapter(
                context as Context,
                R.layout.support_simple_spinner_dropdown_item,
                Category.getSubcategories(it.category)
            )
            val subCatIndex = Category.getSubcategories(it.category)
                .indexOfFirst { subcat -> subcat == it.subcategory }
            if (subCatIndex != -1) {
                subcategoriesSpinner.setSelection(subCatIndex, true)
            }

            if (it.likedPeopleList == null || it.likedPeopleList.size == 0) {
                sellBtn.visibility = View.GONE
            }

            if (!it.photo.isNullOrEmpty()) {
                setItemImage(it.photo!!)
            }
//        if(!it.location.isNullOrEmpty()){

            try {
                val userLocation = it.location.split(",".toRegex()).toTypedArray()
                latLng = LatLng(userLocation[0].toDouble(), userLocation[1].toDouble())
            } catch (e: Exception) {
                Log.e("Warning", "Wrong location format!")
            }

            location = "${latLng.latitude},${latLng.longitude}"

            mapView.getMapAsync {
                try{
                googleMap = it
                val geocoder = Geocoder(activity, Locale.getDefault())
                val addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
                val address = addresses[0].getAddressLine(0)
                itemAddress = address
                requestLocationPermission()
                it.addMarker(MarkerOptions().position(latLng))
                it.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
            }
                catch (e:Exception){
                    Log.d("grpc error","ignore")
                }
            }
        })

        vm.getAdvertisementPicture().observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (!it.isNullOrEmpty()) {
                setItemImage(it)
            }
        })

        if (!vm.getAdvertisementId().isNullOrEmpty()) {
            vm.loadAdvertisementInterestedUsers(vm.getAdvertisementId()!!)
        }

        mapView.getMapAsync(this::onMapReady)
        registerForContextMenu(itemImageButton)
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        menu.setHeaderTitle("Please select an option")
        activity?.menuInflater?.inflate(R.menu.context_menu, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.take_a_picture -> {
                this.dispatchTakePictureIntent()
                return true
            }
            R.id.open_gallery -> {
                this.loadImageFromGallery()
                return true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    private fun loadImageFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity?.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                requestPermissions(permissions, PERMISSIONS_REQUEST_READ_STORAGE)
            } else {
                dispatchPickPictureFromGalleryIntent()
            }
        } else {
            dispatchPickPictureFromGalleryIntent()
        }
    }

    private fun dispatchPickPictureFromGalleryIntent() {
        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.type = "image/*"
        startActivityForResult(photoPickerIntent, REQUEST_IMAGE_GALLERY)
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val pm: PackageManager? = activity?.packageManager
        if (pm?.let { takePictureIntent.resolveActivity(it) } != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        }
    }

    private fun setItemImage(picture: String) {
        picturePath = picture
        if (picturePath.isNotEmpty()) {
            try {
                if (picturePath.startsWith("http")) {
                    Picasso.get().load(picturePath).placeholder(R.drawable.item_placeholder)
                        .error(R.drawable.item_placeholder).into(itemPicture)
                } else {
                    Picasso.get().load(File(picturePath)).placeholder(R.drawable.item_placeholder)
                        .error(R.drawable.item_placeholder).into(itemPicture)
                }
            } catch (e: Exception) {
                Log.d("ENDURANCE", "Couldn't set the user picture. Err=$e")
                itemPicture.setImageResource(R.drawable.item_placeholder)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == AppCompatActivity.RESULT_OK && data != null) {
            val bitmap = data.extras!!["data"] as Bitmap
            storagePath = Image.getImageFromResult(bitmap, requireActivity())
            vm.setAdvertisementPicture(storagePath)
            bitmap.recycle()
        } else if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == AppCompatActivity.RESULT_OK && data != null) {
            val inputStream = activity?.contentResolver?.openInputStream(data.data as Uri)
            val bitmap = BitmapFactory.decodeStream(inputStream)
            inputStream?.close()
            storagePath = Image.getImageFromResult(bitmap, requireActivity())
            vm.setAdvertisementPicture(storagePath)
            bitmap.recycle()
        }
    }

    private fun showDatePicker() {
        val dateValue = expirationInput.text.toString().split("/").toTypedArray()

        // current date
        var year = Calendar.getInstance().get(Calendar.YEAR)
        var month = Calendar.getInstance().get(Calendar.MONTH)
        var dayOfMonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)

        // if we selected a previous date
        if (!dateValue.isNullOrEmpty() && dateValue.size == 3) {
            dayOfMonth = dateValue[0].toInt()
            month = dateValue[1].toInt()
            year = dateValue[2].toInt()
        }

        val datePickerDialog = DatePickerDialog(
            context as Context, this,
            year,
            month,
            dayOfMonth
        )

        // don't allow past dates
        datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000
        datePickerDialog.show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        // NOTE: this fixes a bug where even though past dates are shown as disabled they are still selectable
        val currentYear = Calendar.getInstance().get(Calendar.YEAR)
        val currentMonth = Calendar.getInstance().get(Calendar.MONTH)
        val currentDayOfMonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        if (year == currentYear && month == currentMonth && currentDayOfMonth > dayOfMonth) {
            Toast.makeText(context, "Please select a valid date", Toast.LENGTH_LONG).show()
            return
        }

        expirationInput.setText("${dayOfMonth.toString()}/${month.toString()}/${year.toString()}")
    }

    private fun validateInputs(): Boolean {
        if (titleInput.text.isNullOrEmpty() ||
            descriptionInput.text.isNullOrEmpty() ||
            priceInput.text.isNullOrEmpty() ||
            categoriesSpinner.selectedItem.toString().isEmpty() ||
            subcategoriesSpinner.selectedItem.toString().isEmpty() ||
            expirationInput.text.isNullOrEmpty()
        ) {
            return false
        }

        return true
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.edit_item_menu, menu)
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        Keyboard.hide(requireActivity())

        if (vm.getIsSaving()) {
            return false
        }

        if (menuItem.itemId == R.id.saveItemMenuItem) {
            val isValidItem = validateInputs()
            if (!isValidItem) {
                Snackbar.make(
                    view as View,
                    "Please fill out all the fields.",
                    Snackbar.LENGTH_SHORT
                ).show()
                return false
            }

            progressBar.visibility = View.VISIBLE

            Log.d("address:", itemAddress)
//            vm.setAddress(itemAddress)
//            vm.setLocation(location)
            // new item added
            if (vm.getAdvertisementId().isNullOrEmpty()) {
                val advertisement = AdvertisementItem(
                    vm.getAdvertisementPushID(),
                    titleInput.text.toString(),
                    descriptionInput.text.toString(),
                    priceInput.text.toString().toDouble(),
                    categoriesSpinner.selectedItem.toString(),
                    subcategoriesSpinner.selectedItem.toString(),
                    location,
                    itemAddress,
                    expirationInput.text.toString(),
                    picturePath,
                    Auth.getCurrentUserID(),
                    false,
                    isBlockedSwitch.isChecked
                )

                vm.addAdvertisement(advertisement) {
                    progressBar.visibility = View.GONE
                    vm.setIsSaving(false)
                    findNavController().popBackStack()
                    Snackbar.make(
                        view as View,
                        "Item was created successfully.",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
            } else {
                val updates = mutableMapOf<String, Any?>(
                    "id" to vm.getAdvertisementId()!!,
                    "title" to titleInput.text.toString(),
                    "description" to descriptionInput.text.toString(),
                    "price" to priceInput.text.toString().toDouble(),
                    "category" to categoriesSpinner.selectedItem.toString(),
                    "subcategory" to subcategoriesSpinner.selectedItem.toString(),
                    "location" to location,
                    "address" to itemAddress,
                    "expirationDate" to expirationInput.text.toString(),
                    "photo" to picturePath,
                    "blocked" to isBlockedSwitch.isChecked,
                    "updatedAt" to System.currentTimeMillis() / 1000
                )
                // item was edited
                vm.updateAdvertisement(updates) {
                    progressBar.visibility = View.GONE
                    vm.setIsSaving(false);
                    if (it) {
                        if (vm.getIsFromListPage()) {
                            findNavController().navigate(R.id.action_item_edit_fragment_to_nav_item_list)
                        } else {
                            findNavController().popBackStack()
                        }

                        Snackbar.make(
                            view as View,
                            "Item was updated successfully.",
                            Snackbar.LENGTH_SHORT
                        ).show()
                    } else {
                        Snackbar.make(
                            view as View,
                            "Opsss. Couldn't update item. Please try again later.",
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        } else {
            findNavController().popBackStack()
        }

        return true
    }

    private fun sellItem() {
        vm.getAdvertisementInterestedUsers()
            .observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                if (it != null && it.isNotEmpty()) {
                    sellItemDialogFragment = SellItemDialogFragment(this, it)
                    Log.d("dasdasd", "sellItem: is visible ${sellItemDialogFragment.isVisible}")
                    activity?.supportFragmentManager?.let { sfm ->
                        sellItemDialogFragment.show(
                            sfm,
                            "SellItemDialogFragment"
                        )
                    }
                }
            })
    }

    override fun onSell(user: User) {
        vm.sellItem(user.id)
            .addOnSuccessListener {
                Snackbar.make(
                    view as View,
                    "Item sold. All the interested persons will be notified.",
                    Snackbar.LENGTH_SHORT
                ).show()
                findNavController().navigate(R.id.action_item_edit_fragment_to_nav_item_list)
            }
            .addOnFailureListener {
                // show error to the user.
                Log.d("error", it.toString())
                Snackbar.make(
                    view as View,
                    "Opsss. Something went wrong. Please try to sell this item again later.",
                    Snackbar.LENGTH_SHORT
                ).show()
            }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        if (PERMISSIONS_REQUEST_READ_STORAGE == requestCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchPickPictureFromGalleryIntent()
            }
        } else if (PERMISSIONS_LOCATION == requestCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestLocationUpdate()
            }
        } else {
            Toast.makeText(activity, "Permission denied", Toast.LENGTH_SHORT).show()
        }
    }

    @SuppressLint("WrongConstant")
    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestLocationPermission() {
        Log.d("perpission",ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION).toString())
        when (PERMISSIONS_LOCATION) {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) -> requestLocationUpdate()
            else -> {
                Log.d("perpission2",ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION).toString())
                val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
                requestPermissions(permissions, PERMISSIONS_LOCATION)
            }
        }
    }
    @SuppressLint("MissingPermission")
    @RequiresApi(Build.VERSION_CODES.M)
    private fun onMapReady(map: GoogleMap) {
        googleMap = map
        requestLocationPermission()

        map.setOnMapClickListener {
            try {
                val geocoder = Geocoder(activity, Locale.getDefault())
                val addresses = geocoder.getFromLocation(it.latitude, it.longitude, 1)
                val address = addresses[0].getAddressLine(0)
                itemAddress = address
                Toast.makeText(
                    activity,
                    address,
                    Toast.LENGTH_SHORT
                ).show()
                locationTextView2.text = address
                googleMap?.clear()
                googleMap?.addMarker(MarkerOptions().position(it).title(address));
                location = "${it.latitude},${it.longitude}"
            } catch (e: Exception) {
                Log.e("Error", e.stackTrace.toString())
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestLocationUpdate() {

        handleLocation()

    }

    @SuppressLint("MissingPermission")
    private fun handleLocation() {
        Log.d("getLocation", "handle")
        val map = googleMap ?: return
        map.isMyLocationEnabled = true
        map.uiSettings?.isMyLocationButtonEnabled = true
    }

    override fun onResume() {
        super.onResume()
        mapView4?.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView4?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView4?.onStop()
    }

    override fun onPause() {
        super.onPause()
        mapView4?.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView4?.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView4?.onDestroy()
    }
}