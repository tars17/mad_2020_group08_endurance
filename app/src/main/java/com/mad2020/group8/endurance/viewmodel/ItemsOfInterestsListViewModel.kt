package com.mad2020.group8.endurance.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.repository.InterestedItemsRepository

class ItemsOfInterestsListViewModel : ViewModel() {

    private val items: MutableLiveData<List<AdvertisementItem>> = InterestedItemsRepository.getItemsOfInterest()

    fun getItems(): MutableLiveData<List<AdvertisementItem>> {
        return items
    }

    fun removeItem(itemID: String) {
        // remove from interests
        InterestedItemsRepository.removeItemFromInterests(itemID)
    }
}
