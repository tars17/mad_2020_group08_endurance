package com.mad2020.group8.endurance.ui.item_on_sale_list

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.SeekBar
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.data.ItemFilter
import com.mad2020.group8.endurance.helper.Category
import kotlinx.android.synthetic.main.fragment_on_sale_items_filters_dialog.view.*
import kotlinx.android.synthetic.main.fragment_on_sale_items_filters_dialog.view.categoriesSpinner

class OnSaleItemsFiltersDialogFragment(private val listener: OnSaleItemsFiltersDialogListener, private val filter: ItemFilter) : DialogFragment() {

    interface OnSaleItemsFiltersDialogListener {
        fun onFilterClick(filter: ItemFilter)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            val view = inflater.inflate(R.layout.fragment_on_sale_items_filters_dialog, null)
            ArrayAdapter.createFromResource(
                context as Context,
                R.array.item_categories,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                view.categoriesSpinner.adapter = adapter
            }

            view.searchInput.setText(filter.text)
            view.searchInput.setSelection(view.searchInput.text?.length ?: 0)
            view.seekBar.progress = filter.price / 50
            view.filtersPriceLabel.text = getPriceText(view.seekBar.progress)

            var categoryIndex = Category.getCategories().indexOfFirst { cat -> cat == filter.category }
            if (categoryIndex == 0) {
                categoryIndex = 1
            }

            if (categoryIndex == -1) {
                categoryIndex = 0
            }

            view.categoriesSpinner.setSelection(categoryIndex)

            view.seekBar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    view.filtersPriceLabel.text = getPriceText(progress)
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {}
                override fun onStopTrackingTouch(seekBar: SeekBar?) {}
            })

            builder.setView(view)
                .setPositiveButton("Filter") { _, _ ->
                    listener.onFilterClick(
                        ItemFilter(
                            view.searchInput.text.toString(),
                            view.seekBar.progress * 50,
                            view.categoriesSpinner.selectedItem.toString()
                        )
                    )
                }
                .setNegativeButton("Cancel") { _, _ -> dialog?.cancel() }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    fun getPriceText(price: Int): String {
        if (price > 0) {
            return "Price up to: $${price * 50}"
        }

        return "Filter by price"
    }

}
