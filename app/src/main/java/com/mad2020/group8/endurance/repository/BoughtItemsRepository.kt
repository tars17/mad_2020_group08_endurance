package com.mad2020.group8.endurance.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.helper.Auth

object BoughtItemsRepository {
    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()

    fun getItemsBought(): MutableLiveData<List<AdvertisementItem>> {
        val currentUserID = Auth.getCurrentUserID();
        val data = MutableLiveData<List<AdvertisementItem>>()

        db.collection(FirestoreCollections.USERS_ITEMS_BOUGHT.value)
            .document(currentUserID)
            .addSnapshotListener { snap, err ->
                if (err != null) {
                    Log.d("Error", err.toString())
                }

                if (snap != null ) {
                    val keys = snap.data?.keys?.toMutableList() ?: emptyList<String>()
                    if (keys.isNotEmpty()) {
                        db.collection(FirestoreCollections.ADVERTISEMENTS.value)
                            .whereIn("id", keys)
                            .addSnapshotListener { itemsSnap, itemsErr ->
                                if (itemsErr != null) {
                                    Log.d("Error", itemsErr.toString())
                                }

                                val itemsList: ArrayList<AdvertisementItem> = ArrayList()
                                if (itemsSnap != null ) {
                                    for (item in itemsSnap.documents) {
                                        val i = item.toObject(AdvertisementItem::class.java)?.apply { id = item.id }
                                        if (i != null) {
                                            itemsList.add(i)
                                        }
                                    }
                                }

                                data.value = itemsList
                            }
                    } else {
                        data.value = emptyList()
                    }
                }
            }

        return data
    }

}
