package com.mad2020.group8.endurance.data

data class User (
    var id: String = "",
    var fullname: String = "",
    var nickname: String = "",
    var email: String = "",
    var location: String = "",
    var token: String = "",
    var picture: String? = null,
    var score: Double ?= 0.0,
    var peoplenum: Int ?=0,
    val commentList: ArrayList<String>? =  arrayListOf()
)