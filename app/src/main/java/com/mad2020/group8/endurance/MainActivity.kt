package com.mad2020.group8.endurance

import android.animation.ObjectAnimator
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.navigation.NavigationView
import com.google.firebase.iid.FirebaseInstanceId
import com.mad2020.group8.endurance.helper.Auth
import com.mad2020.group8.endurance.repository.UserRepository
import com.mad2020.group8.endurance.viewmodel.UsersViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*


class MainActivity : AppCompatActivity(), FragmentManager.OnBackStackChangedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var header: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.elevation = 0F
        setSupportActionBar(toolbar)
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        header = navView.getHeaderView(0)
        startService(Intent(this@MainActivity, GetLocationService::class.java))
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_on_sale_list,
                R.id.nav_item_list,
                R.id.nav_interested_items,
                R.id.nav_bought_items
            ), drawerLayout
        )

        logoutBtn.setOnClickListener {
            Auth.logout()
            Intent(this, LoginActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(this)
                finish()
            }
        }
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        val vm: UsersViewModel = ViewModelProvider(this).get(UsersViewModel::class.java)
        vm.getCurrentUser().observe(this, Observer {
            if (it != null) {
                header.userNameDrawerTextView.text = it.fullname

                if (!it.picture.isNullOrEmpty()) {
                   Log.d("main",it.picture.toString())
                    // TODO: try catch this as it might throw
                    Picasso.get().load(it.picture).into(header.userPictureDrawer)
                } else {
                    Picasso.get().load(R.drawable.avatar_placeholder).into(header.userPictureDrawer)
                }
            }
        })

        this.listenToFirebaseToken()
    }

    override fun onBackStackChanged() {
        super.onBackPressed()
        overridePendingTransition(0,0)
    }

    private fun listenToFirebaseToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token
                if (token != null) {
                    UserRepository.updateFCMToken(token) {
                        Log.d("Token", "Updated token $token")
                    }
                }
            })
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}
