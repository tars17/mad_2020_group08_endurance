package com.mad2020.group8.endurance.ui.liked_people_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.adapter.UserAdapter
import com.mad2020.group8.endurance.data.User
import com.mad2020.group8.endurance.viewmodel.LikedPeopleListViewModel
import com.mad2020.group8.endurance.viewmodel.LikedPeopleListViewModelFactory
import kotlinx.android.synthetic.main.fragment_liked_people_list.*

class LikedPeopleListFragment: Fragment() {

    private lateinit var vm: LikedPeopleListViewModel
    private lateinit var vmFactory: LikedPeopleListViewModelFactory
    private lateinit var adapter: UserAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        vmFactory = LikedPeopleListViewModelFactory(arguments?.get("likedPeopleList") as List<String>)
        vm = ViewModelProvider(this, vmFactory).get(LikedPeopleListViewModel::class.java)
        return inflater.inflate(R.layout.fragment_liked_people_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = UserAdapter(emptyList())
        adapter.onPersonClick = { user, position -> onPersonClick(user, position) }

        LikedPeopleListRecyclerView.layoutManager = LinearLayoutManager(context)
        LikedPeopleListRecyclerView.adapter = adapter

        vm.getUsers().observe(viewLifecycleOwner, Observer {
            adapter.setUsers(it)
        })
    }

    private fun onPersonClick(person: User, position: Int) {
       //load to the page of specified user
       findNavController().navigate(
           R.id.action_liked_people_list_fragment_to_nav_show_profile,
           bundleOf("userID" to person.id, "hideHomeMenu" to true)
       )
    }

}