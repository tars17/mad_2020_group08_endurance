package com.mad2020.group8.endurance.helper

import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.os.Environment
import android.util.Log
import androidx.fragment.app.FragmentActivity
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

object Image {

    fun handleImageRotation(imagePath: String, bitmap: Bitmap): Bitmap? {
        val ei = ExifInterface(imagePath)
        val orientation: Int = ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_NORMAL
        )

        return when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap, 90)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap, 180)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap, 270)
            ExifInterface.ORIENTATION_NORMAL -> bitmap
            else -> bitmap
        }
    }

    fun rotateImage(img: Bitmap, degree: Int): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(degree.toFloat())
        return Bitmap.createBitmap(img, 0, 0, img.width, img.height, matrix, true)
    }

    @Throws(IOException::class)
    fun createImageFile(activity: FragmentActivity): String {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)

        return File.createTempFile("IMAGE_${timeStamp}_", ".jpg", storageDir).absolutePath;
    }

    fun getImageFromResult(bitmap: Bitmap, activity: FragmentActivity): String {
        // save the image first
        var imagePath = "";
        try {
            val tempPath = createImageFile(activity)
            if (!com.mad2020.group8.endurance.helper.File.bitmapToFile(bitmap, tempPath)) {
                Log.d("ENDURANCE", "Couldn't save the bitmap to file.")
            }

            val rotatedBitmap = handleImageRotation(tempPath, bitmap)
            // update the rotated image
            if (!com.mad2020.group8.endurance.helper.File.bitmapToFile(
                    rotatedBitmap as Bitmap,
                    tempPath
                )
            ) {
                Log.d("ENDURANCE", "Couldn't save the rotated bitmap.")
            }

            rotatedBitmap.recycle()
            imagePath = tempPath
        } catch (ex: IOException) {
            // Error occurred while creating the File
            Log.d("ENDURANCE", "Couldn't create file after taking picture. Err=$ex")
        }

        return imagePath
    }
}