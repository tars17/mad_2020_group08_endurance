package com.mad2020.group8.endurance.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mad2020.group8.endurance.data.User
import com.mad2020.group8.endurance.helper.Auth
import com.mad2020.group8.endurance.repository.UserRepository

class EditProfileViewModel: ViewModel() {

    private val user: MutableLiveData<User> = UserRepository.getById(Auth.getCurrentUserID()) as MutableLiveData<User>
    private var isSaving: Boolean = false

    fun setTempPicture(picture: String) {
        val updatedUser = user.value
        updatedUser?.picture = picture
        user.value = updatedUser
    }

    fun setLocation(location:String) {
        val updatedUser = user.value
        updatedUser?.location = location
        user.value = updatedUser
    }

    fun getProfile(): LiveData<User> {
        return user
    }

    fun updateProfile(callback: (success: Boolean) -> Unit) {
        this.user.value?.let {
            setIsSaving(true)
            UserRepository.updateUser(it, callback)
        }
    }

    fun getIsSaving(): Boolean {
        return isSaving
    }

    fun setIsSaving(status: Boolean) {
        this.isSaving = status
    }
}