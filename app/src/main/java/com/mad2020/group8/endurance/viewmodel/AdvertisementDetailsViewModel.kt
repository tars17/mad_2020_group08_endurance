package com.mad2020.group8.endurance.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.repository.AdvertisementRepository
import com.mad2020.group8.endurance.repository.InterestedItemsRepository

class AdvertisementDetailsViewModel(private val itemID: String): ViewModel() {
    private var item: MutableLiveData<AdvertisementItem> = AdvertisementRepository.getItemAndSubscribe(itemID)

    fun getItem(): MutableLiveData<AdvertisementItem> {
        return item
    }

    fun addLikedPerson(advertisementItem: AdvertisementItem){
        item.value?.let{
            // add inside item document
            AdvertisementRepository.updateItem(this.itemID, mapOf("likedPeopleList" to advertisementItem.likedPeopleList)){}
            // add inside interested items document
            InterestedItemsRepository.addItemToInterests(itemID)
        }
    }

    fun removeItem(itemID: String) {
        //remove from interests
        InterestedItemsRepository.removeItemFromInterests(itemID)
    }
}