package com.mad2020.group8.endurance.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class LikedPeopleListViewModelFactory(private val userIds: List<String>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LikedPeopleListViewModel::class.java)) {
            return LikedPeopleListViewModel(userIds) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}