package com.mad2020.group8.endurance.ui.item_edit

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.data.User
import kotlinx.android.synthetic.main.fragment_sell_item_dialog.view.*

class SellItemDialogFragment(private val listener: SellItemDialogListener, private val users: List<User>) : DialogFragment() {

    interface SellItemDialogListener {
        fun onSell(user: User)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            val view = inflater.inflate(R.layout.fragment_sell_item_dialog, null)

            val adapter = ArrayAdapter(context as Context, android.R.layout.simple_list_item_1, users.map { user -> user.fullname })
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            view.usersSpinner.adapter = adapter

            builder.setView(view)
                .setPositiveButton("Sell") { _, _ ->
                    val selectedUser = users[view.usersSpinner.selectedItemPosition]
                    listener.onSell(selectedUser)
                }
                .setNegativeButton("Cancel") { _, _ -> dialog?.cancel() }

            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

}
