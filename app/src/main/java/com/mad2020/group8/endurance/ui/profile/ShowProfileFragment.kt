package com.mad2020.group8.endurance.ui.profile

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.core.view.marginTop
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.mad2020.group8.endurance.MainActivity
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.helper.Auth
import com.mad2020.group8.endurance.map.LocationApplication
import com.mad2020.group8.endurance.viewmodel.UsersViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_show_profile.*
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread
import java.lang.Boolean.FALSE
import java.lang.Boolean.TRUE
import java.util.*
import org.jetbrains.anko.uiThread


class ShowProfileFragment : Fragment() {
    private lateinit var vm: UsersViewModel
    private var currentUser: Boolean = true
    private var googleMap: GoogleMap? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private val PERMISSIONS_LOCATION = 3

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        (activity as MainActivity).supportActionBar?.title = "My profile"
        vm = ViewModelProvider(this).get(UsersViewModel::class.java)

        try {
            if (Auth.getCurrentUserID() != arguments?.get("userID") as String) {
                currentUser = false
            }
        } catch (e: Exception) {
            Log.e("ENDURANCE", "No userID input")
        }

        val hideHomeMenuIcon = arguments?.getBoolean("hideHomeMenu") ?: false
        if (!hideHomeMenuIcon) {
            (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
            (activity as MainActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline)
        }

        return inflater.inflate(R.layout.fragment_show_profile, container, false)
    }

    @SuppressLint("ResourceType")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lateinit var userID: String
        fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(activity as MainActivity)
        locationRequest = LocationRequest().apply {
            Log.d("Location???", "request")

            interval = 5000  //请求时间间隔
            fastestInterval = 3000 //最快时间间隔
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                Log.d("Location???", "callback")

                locationResult ?: return

                handleLocation(locationResult.lastLocation)
            }
        }


        val mapView: MapView = view.findViewById(R.id.mapView)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this::onMapReady)


        if (currentUser) {
            vm.getCurrentUser().observe(viewLifecycleOwner, Observer {
                userID = it.id
                fullnameText.text = it.fullname
                nicknameText.text = it.nickname
                emailText.text = it.email

                var latLng = LatLng(42.00, 12.00)
                try {
                    val userLocation = it.location.split(",".toRegex()).toTypedArray()
                    latLng = LatLng(userLocation[0].toDouble(), userLocation[1].toDouble())
                } catch (e: Exception) {
                    Log.e("Warning", "Wrong location format!")
                }
                mapView.getMapAsync {
                    googleMap = it
//        drawRoutes(map)
                    try {
                        val geocoder = Geocoder(activity, Locale.getDefault())
                        val addresses =
                            geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
                        val address = addresses[0].getAddressLine(0)
                        it.addMarker(MarkerOptions().position(latLng).title(address))
                            .showInfoWindow()
                        it.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
                    }catch (e:Exception){

                    }
                }

                if (it.score == 0.0) {
                    ratingBar.visibility = View.GONE
                } else {
                    ratingBar.rating = it.score!!.toFloat()
                }
                if (it.commentList?.isEmpty()!!) {
                    showCommentBtn.visibility = View.GONE
                }

                if (!it.picture.isNullOrEmpty()) {
                    Picasso.get()
                        .load(it.picture)
                        .placeholder(R.drawable.avatar_placeholder)
                        .error(R.drawable.avatar_placeholder).into(userPicture)
                } else {
                    Picasso.get()
                        .load(R.drawable.avatar_placeholder)
                        .into(userPicture)
                }
            })
        } else {
            vm.getUser(arguments?.get("userID") as String).observe(viewLifecycleOwner, Observer {
                userID = arguments?.get("userID") as String

                fullnameText.text = it.fullname
                emailText.text = it.email
                nicknameText.visibility = View.INVISIBLE
                nicknameText.height = 1

                (activity as MainActivity).supportActionBar?.title = "${it.fullname}'s profile"

                var latLng = LatLng(42.00, 12.00)
                try {
                    val userLocation = it.location.split(",".toRegex()).toTypedArray()
                    latLng = LatLng(userLocation[0].toDouble(), userLocation[1].toDouble())
                } catch (e: Exception) {
                    Log.e("Warning", "Wrong location format!")
                }

                mapView.getMapAsync {
                    googleMap = it
                    try {
                        val geocoder = Geocoder(activity, Locale.getDefault())
                        val addresses =
                            geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
                        val address = addresses[0].getAddressLine(0)
                        it.addMarker(MarkerOptions().position(latLng).title(address))
                            .showInfoWindow();
                        it.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
                    } catch (e: Exception) {
                        Snackbar.make(view as View, "Too far to reach there.", Snackbar.LENGTH_SHORT).show()
                        findNavController().navigate(R.id.action_showProfileFragment_to_editProfileFragment)

                        Log.e("Error", "no connection")
                    }
                }


                if (it.score == 0.0) {
                    ratingBar.visibility = View.GONE
                    showCommentBtn.visibility = View.GONE
                } else {
                    ratingBar.rating = it.score!!.toFloat()
                }
                if (!it.picture.isNullOrEmpty()) {
                    Picasso.get().load(it.picture).placeholder(R.drawable.avatar_placeholder)
                        .error(R.drawable.avatar_placeholder).into(userPicture)
                } else {
                    Picasso.get().load(R.drawable.avatar_placeholder).into(userPicture)
                }
            })
        }

        showCommentBtn.setOnClickListener {
            findNavController().navigate(
                R.id.action_showProfileFragment_to_commentListFragment,
                bundleOf("userID" to userID)
            )
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        if (currentUser) {
            super.onCreateOptionsMenu(menu, inflater)
            inflater.inflate(R.menu.show_profile_menu, menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.editProfileMenuItem) {
            editProfile()
        } else {
            val hideHomeMenuIcon = arguments?.getBoolean("hideHomeMenu") ?: false
            if (hideHomeMenuIcon) {
                findNavController().popBackStack()
            } else {
                activity?.findViewById<DrawerLayout>(R.id.drawer_layout)?.openDrawer(GravityCompat.START)
            }
        }

        return true
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun onMapReady(map: GoogleMap) {
        googleMap = map
        requestLocationPermission()
    }

    @SuppressLint("MissingPermission")
    private fun requestLocationUpdate() {
        if (LocationApplication().getLocation() == null) {
            Toast.makeText(activity, "Can't find your location, please try again!", Toast.LENGTH_SHORT).show()
//            findNavController().navigate(
//                R.id.action_map_to_item_details_fragment,
//                bundleOf("itemID" to arguments?.getString("itemID"))
//            )
            findNavController().popBackStack()
            return
        }
        handleLocation(LocationApplication().getLocation()!!)
    }

    @SuppressLint("MissingPermission")
    private fun handleLocation(location: Location) {
        val map = googleMap ?: return
        map?.isMyLocationEnabled = true
        map?.uiSettings?.isMyLocationButtonEnabled = true
        stopLocationUpdates()
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    @SuppressLint("WrongConstant")
    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestLocationPermission() {
        when (PERMISSIONS_LOCATION) {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) -> requestLocationUpdate()
            else -> {
                val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
                requestPermissions(permissions, PERMISSIONS_LOCATION)
            }
        }
    }

    private fun editProfile() {
        findNavController().navigate(R.id.action_showProfileFragment_to_editProfileFragment)
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }
}
