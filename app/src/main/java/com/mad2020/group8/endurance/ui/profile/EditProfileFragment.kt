package com.mad2020.group8.endurance.ui.profile

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.mad2020.group8.endurance.MainActivity
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.databinding.FragmentEditProfileBinding
import com.mad2020.group8.endurance.helper.Image
import com.mad2020.group8.endurance.helper.Keyboard
import com.mad2020.group8.endurance.map.LocationApplication
import com.mad2020.group8.endurance.viewmodel.EditProfileViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import java.util.*
import kotlin.collections.ArrayList

class EditProfileFragment : Fragment() {
    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_IMAGE_GALLERY = 0
    private val PERMISSIONS_REQUEST_READ_STORAGE = 2
    private val PERMISSIONS_LOCATION = 3
    private lateinit var vm: EditProfileViewModel
    private var picturePath: String = ""
    private var storagePath: String = ""

    private var googleMap: GoogleMap? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private var latLng = LatLng(42.00, 12.00)
    private val locationPermission = Manifest.permission.ACCESS_FINE_LOCATION
    private val permissions =
        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE)
    val mPermissionList = ArrayList<String>()
    val mRequestCode = 0x1//权限请求码

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as MainActivity).supportActionBar?.title = "Edit profile"
        setHasOptionsMenu(true)
        vm = ViewModelProvider(this).get(EditProfileViewModel::class.java)

        val binding: FragmentEditProfileBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_edit_profile, container, false)
        binding.viewmodel = vm
        binding.lifecycleOwner = this

        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(activity as MainActivity)
        locationRequest = LocationRequest().apply {
            interval = 5000  //请求时间间隔
            fastestInterval = 3000 //最快时间间隔
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                Log.d("Location???", "callback")
                locationResult ?: return
                handleLocation(locationResult.lastLocation)
            }
        }


        val mapView: MapView = view.findViewById(R.id.mapView2)
        mapView.onCreate(savedInstanceState)
        mapView.isClickable = true

        vm.getProfile().observe(viewLifecycleOwner, Observer {

            if (!it.location.isNullOrEmpty()) {

                try {
                    val userLocation = it.location.split(",".toRegex()).toTypedArray()
                    latLng = LatLng(userLocation[0].toDouble(), userLocation[1].toDouble())
                } catch (e: Exception) {
                    Log.e("Warning", "Wrong location format!")
                }
                mapView.getMapAsync {
                    googleMap = it
                    requestLocationPermission()
                    it.addMarker(MarkerOptions().position(latLng))
                    it.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
                }
            }
            if (!it.picture.isNullOrEmpty()) {
                setUserPicture(it.picture!!)
            }
        })
        mapView.getMapAsync(this::onMapReady)
        //        location.init(view,savedInstanceState,activity as MainActivity)

        registerForContextMenu(userPicture)
    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun onMapReady(map: GoogleMap) {
        googleMap = map
        requestLocationPermission()

        map.setOnMapClickListener {
            try {
                val geocoder = Geocoder(activity, Locale.getDefault())
                val addresses = geocoder.getFromLocation(it.latitude, it.longitude, 1)
                val address = addresses[0].getAddressLine(0)

                Toast.makeText(
                    activity, address,
                    Toast.LENGTH_SHORT
                ).show()
                googleMap?.clear()
                googleMap?.addMarker(MarkerOptions().position(it).title(address));
                vm.setLocation("${it.latitude},${it.longitude}")
            } catch (e: Exception) {
                Log.e("Error", "no connection")
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestLocationUpdate() {

        handleLocation(LocationApplication().getLocation())

    }

    @SuppressLint("MissingPermission")
    private fun handleLocation(location: Location?) {
        val map = googleMap ?: return
        map?.isMyLocationEnabled = true
        map?.uiSettings?.isMyLocationButtonEnabled = true
    }


    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        menu.setHeaderTitle("Please select an option")
        activity?.menuInflater?.inflate(R.menu.context_menu, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.take_a_picture -> {
                this.dispatchTakePictureIntent()
                return true
            }
            R.id.open_gallery -> {
                this.loadImageFromGallery()
                return true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    private fun loadImageFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity?.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                requestPermissions(permissions, PERMISSIONS_REQUEST_READ_STORAGE)
            } else {
                dispatchPickPictureFromGalleryIntent()
            }
        } else {
            dispatchPickPictureFromGalleryIntent()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        if (PERMISSIONS_REQUEST_READ_STORAGE == requestCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchPickPictureFromGalleryIntent()
            }
        } else if (PERMISSIONS_LOCATION == requestCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestLocationUpdate()
            }
        } else {
            Toast.makeText(activity, "Permission denied", Toast.LENGTH_SHORT).show()
        }
    }


    private fun dispatchPickPictureFromGalleryIntent() {
        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.type = "image/*"
        startActivityForResult(photoPickerIntent, REQUEST_IMAGE_GALLERY)
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val pm: PackageManager? = activity?.packageManager

        if (pm?.let { takePictureIntent.resolveActivity(it) } != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        }
    }

    private fun setUserPicture(picturePath: String) {
        this.picturePath = picturePath
        try {
            if (picturePath.startsWith("http")) {
                val picture = view?.findViewById(R.id.userPicture) as ImageView
                Picasso.get().load(picturePath).placeholder(R.drawable.avatar_placeholder)
                    .error(R.drawable.avatar_placeholder).into(picture)
            } else {
                userPicture.setImageURI(Uri.parse(picturePath))
            }
        } catch (e: Exception) {
            Log.e("ENDURANCE", "Couldn't set the user picture. Err=$e")
            userPicture.setImageResource(R.drawable.avatar_placeholder)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == AppCompatActivity.RESULT_OK && data != null) {
            val bitmap = data.extras!!["data"] as Bitmap
            storagePath = Image.getImageFromResult(bitmap, requireActivity())
            vm.setTempPicture(storagePath)
            bitmap.recycle()
        } else if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == AppCompatActivity.RESULT_OK && data != null) {
            val inputStream = activity?.contentResolver?.openInputStream(data.data as Uri)
            val bitmap = BitmapFactory.decodeStream(inputStream)
            inputStream?.close()
            storagePath = Image.getImageFromResult(bitmap, requireActivity())
            vm.setTempPicture(storagePath)
            bitmap.recycle()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.edit_profile_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        Keyboard.hide(requireActivity())

        if (vm.getIsSaving()) {
            return false
        }

        if (item.itemId == R.id.saveProfileMenuItem) {
            val isValid = validateInputs()

            if (!isValid) {
                Snackbar.make(
                    view as View,
                    "Please fill out all the fields.",
                    Snackbar.LENGTH_SHORT
                ).show()
                return false
            }

            vm.setLocation("${latLng.latitude},${latLng.longitude}")
            progressBar.visibility = View.VISIBLE
            vm.updateProfile {
                progressBar.visibility = View.GONE
                vm.setIsSaving(false)
                findNavController().navigate(R.id.action_editProfileFragment_to_showProfileFragment)
            }

            return true
        } else {
            findNavController().navigate(R.id.action_editProfileFragment_to_showProfileFragment)
            return true
        }
    }

    private fun validateInputs(): Boolean {
        if (fullnameInput.text.isNullOrEmpty() ||
            nicknameInput.text.isNullOrEmpty() ||
            emailInput.text.isNullOrEmpty()
        ) {
            return false
        }

        return true
    }

    @SuppressLint("WrongConstant")
    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestLocationPermission() {
        when (PERMISSIONS_LOCATION) {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) -> requestLocationUpdate()
            else -> {
                val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
                requestPermissions(permissions, PERMISSIONS_LOCATION)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mapView2?.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView2?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView2?.onStop()
    }

    override fun onPause() {
        super.onPause()
        mapView2?.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView2?.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView2?.onDestroy()
    }
}
