package com.mad2020.group8.endurance.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.mad2020.group8.endurance.data.User
import com.mad2020.group8.endurance.repository.UserRepository
import com.mad2020.group8.endurance.repository.AdvertisementRepository

class RatingOwnerViewModel: ViewModel()  {
    fun getOwner(userId: String): LiveData<User> {
        return UserRepository.getByIdAndListen(userId)
    }

    fun updateOwner(user:User) {
        UserRepository.updateUser(user){}
    }
    fun updateupdateItemSoldedStatus(itemID:String)
    {

        AdvertisementRepository.updateupdateItemSoldedStatus(itemID)
    }
}