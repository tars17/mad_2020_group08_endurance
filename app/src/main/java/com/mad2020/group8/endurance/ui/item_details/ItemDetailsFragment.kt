package com.mad2020.group8.endurance.ui.item_details

import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap.OnMapClickListener
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.mad2020.group8.endurance.MainActivity
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.helper.Auth
import com.mad2020.group8.endurance.viewmodel.AdvertisementDetailsViewModel
import com.mad2020.group8.endurance.viewmodel.AdvertisementDetailsViewModelFactory
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_item_details.*
import java.io.File
import java.text.DecimalFormat
import java.util.*


class ItemDetailsFragment : Fragment() {
    private lateinit var vm: AdvertisementDetailsViewModel
    private lateinit var vmFactory: AdvertisementDetailsViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        vmFactory = AdvertisementDetailsViewModelFactory(arguments?.get("itemID") as String)
        vm = ViewModelProvider(this, vmFactory).get(AdvertisementDetailsViewModel::class.java)
        return inflater.inflate(R.layout.fragment_item_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lateinit var newItem: AdvertisementItem
        val itemPic = view.findViewById<ImageView>(R.id.itemdetailsPhotoImageView)
        val mapView: MapView = view.findViewById(R.id.mapView3)
        mapView.onCreate(savedInstanceState)

        vm.getItem().observe(viewLifecycleOwner, Observer {
            (activity as MainActivity).supportActionBar?.title = it.title
            titleTextView.text = it.title
            descriptionTextView.text = it.description
            priceTextView.text = DecimalFormat("#.##").format(it.price).toString() + "$"
            categoryTextView.text = it.category
            subCategoryTextView.text = it.subcategory
            expirationDateTextView.text = it.expirationDate
            locationTextView.text = it.address
            var latLng = LatLng(39.937795, 116.387224)
            try {
                val userLocation = it.location.split(",".toRegex()).toTypedArray()
                latLng = LatLng(userLocation[0].toDouble(), userLocation[1].toDouble())
            } catch (e: Exception) {
                Log.e("Warning", "Wrong location format!")
            }
            mapView.getMapAsync {
                try {
                    val geocoder = Geocoder(activity, Locale.getDefault())
                    val addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
                    val address = addresses[0].getAddressLine(0)
                    it.addMarker(MarkerOptions().position(latLng).title(address)).showInfoWindow();
                    it.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
                    it.setOnMapClickListener(OnMapClickListener {
                        findNavController().navigate(
                            R.id.action_item_details_fragment_to_map,
                            bundleOf("itemID" to arguments?.getString("itemID"))
                        )
                    })
                } catch (e: Exception) {
                    Log.e("Error", "no connection")
                }
            }

            if (Auth.getCurrentUserID() == it.userId) {
                setMenuVisibility(true)
                likeItemFab.visibility = View.GONE
                unlikeItemFab.visibility = View.GONE
                showPeopleLikeBtn.visibility = View.VISIBLE
                ratingBtn.visibility = View.GONE
            } else {
                setMenuVisibility(false)
                val flag = it.likedPeopleList?.find { person -> person == Auth.getCurrentUserID() }

                if (flag == null) {
                    likeItemFab.visibility = View.VISIBLE
                    unlikeItemFab.visibility = View.GONE
                } else {
                    likeItemFab.visibility = View.GONE
                    unlikeItemFab.visibility = View.VISIBLE
                }
            }

            if (it.blocked) {
                notInSaleText.visibility = View.VISIBLE
            }

            if (it.sold) {
                likeItemFab.visibility = View.GONE
                ratingBtn.visibility = View.VISIBLE
            }

            if (it.likedPeopleList == null || it.likedPeopleList.size == 0) {
                showPeopleLikeBtn.visibility = View.GONE
            }

            if (!it.photo.isNullOrEmpty()) {
                if (it.photo!!.startsWith("http")) {
                    Picasso.get().load(it.photo).placeholder(R.drawable.item_placeholder)
                        .error(R.drawable.item_placeholder).into(itemPic)
                } else {
                    Picasso.get().load(File(it.photo!!)).placeholder(R.drawable.item_placeholder)
                        .error(R.drawable.item_placeholder).into(itemPic)
                }
            }

            // TODO: siplify this
            newItem = AdvertisementItem(
                it.id,
                it.title,
                it.description,
                it.price,
                it.category,
                it.subcategory,
                it.location,
                it.address,
                it.expirationDate,
                it.photo,
                it.userId,
                it.sold,
                it.blocked,
                it.createdAt,
                it.updatedAt,
                it.likedPeopleList,
                it.soldToUserId,
                it.rated
            )
        })

        likeItemFab.setOnClickListener {
            val flag =
                newItem.likedPeopleList?.find { person -> person == Auth.getCurrentUserID() }
            if (flag == null) {
                Snackbar.make(
                    view as View,
                    "The owner of this item will be notified about your interest.",
                    Snackbar.LENGTH_SHORT
                ).show()
                newItem.likedPeopleList?.add(Auth.getCurrentUserID())
                vm.addLikedPerson(newItem)
            } else {
                Snackbar.make(
                    view as View,
                    "The owner of this item is already notified about your interest.",
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }

        unlikeItemFab.setOnClickListener{
            //remove item from interests
            vm.removeItem(newItem.id)

            Snackbar.make(
                view as View,
                "You have removed your interest for this item.",
                Snackbar.LENGTH_SHORT
            ).show()
        }

        showPeopleLikeBtn.setOnClickListener {
            if (newItem.likedPeopleList?.size != 0) {
                findNavController().navigate(
                    R.id.action_item_details_fragment_to_liked_people_list_fragment,
                    bundleOf("likedPeopleList" to newItem.likedPeopleList)
                )
            } else {
                Snackbar.make(
                    view as View,
                    "Nobody likes the item at the moment.",
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }

        ratingBtn.setOnClickListener {
            if(!newItem.rated) {
                findNavController().navigate(
                    R.id.action_item_details_fragment_to_rating_owner,
                    bundleOf("userID" to newItem.userId, "itemID" to newItem.id)
                )
            }
            else {
                Snackbar.make(
                    view as View,
                    "You have already rated the owner",
                    Snackbar.LENGTH_SHORT
                ).show()

            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.item_details_menu, menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.editItemMenuItem) {
            Log.d("zeda_test:","back to edit")

            findNavController().navigate(
                R.id.action_item_details_fragment_to_item_edit_fragment,
                bundleOf("itemID" to arguments?.getString("itemID"))
            )
        } else {
            Log.d("zeda_test:","back to nav")
//            findNavController().navigate(R.id.action_item_details_fragment_to_nav_item_list)
            findNavController().popBackStack()
        }

        return true
    }

    override fun onResume() {
        super.onResume()
        mapView3?.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView3?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView3?.onStop()
    }

    override fun onPause() {
        super.onPause()
        mapView3?.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView3?.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView3?.onDestroy()
    }
}