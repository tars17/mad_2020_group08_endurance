package com.mad2020.group8.endurance.ui.comment_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.adapter.CommentAdapter
import com.mad2020.group8.endurance.viewmodel.CommentListViewModel
import kotlinx.android.synthetic.main.fragment_comment_list.*

class CommentListFragment : Fragment() {
    private lateinit var vm: CommentListViewModel
    private lateinit var adapter: CommentAdapter
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        vm = ViewModelProvider(this).get(CommentListViewModel::class.java)
        return inflater.inflate(R.layout.fragment_comment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = CommentAdapter(arrayListOf())

        CommentListRecyclerView.layoutManager = LinearLayoutManager(context)
        CommentListRecyclerView.adapter = adapter

        val userId = arguments?.get("userID") as String
        vm.getUser(userId).observe(viewLifecycleOwner, Observer {
            if(it.commentList?.isEmpty()==false) {
                adapter.setComment(it.commentList)
            }

        })
    }

}