package com.mad2020.group8.endurance.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.Task
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.*
import com.google.firebase.storage.FirebaseStorage
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.data.User
import com.mad2020.group8.endurance.helper.Auth
import java.io.File
import java.io.FileInputStream

object AdvertisementRepository {
    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()
    private var storageRef = FirebaseStorage.getInstance().reference

    fun getItems(): Query {
        return db.collection(FirestoreCollections.ADVERTISEMENTS.value).whereEqualTo("userId", Auth.getCurrentUserID())
    }

    fun getItemsOnSale(): CollectionReference {
        return db.collection(FirestoreCollections.ADVERTISEMENTS.value)
    }

    fun getItem(itemID: String): LiveData<AdvertisementItem> {
        val data = MutableLiveData<AdvertisementItem>()

        db.collection(FirestoreCollections.ADVERTISEMENTS.value)
            .document(itemID)
            .get()
            .addOnSuccessListener {
                data.value = it.toObject(AdvertisementItem::class.java)?.apply { id = it.id }
            }
        return data
    }

    fun getItemAndSubscribe(itemID: String): MutableLiveData<AdvertisementItem> {
        val data = MutableLiveData<AdvertisementItem>()

        db.collection(FirestoreCollections.ADVERTISEMENTS.value)
            .document(itemID)
            .addSnapshotListener { snap, err ->
                if (err != null) {
                    Log.d("Error", err.toString())
                }

                if (snap != null) {
                    data.value = snap.toObject(AdvertisementItem::class.java)?.apply { id = snap.id }
                }
            }

        return data
    }

    fun addItem(item: AdvertisementItem, callback: (success: Boolean) -> Unit) {
        db.collection(FirestoreCollections.ADVERTISEMENTS.value)
            .document(item.id)
            .set(item)
            .addOnSuccessListener {
                callback(true)
            }
            .addOnFailureListener {
                callback(false)
            }
    }

    fun updateItem(advertisementID: String, data: Map<String, Any?>, callback: (success: Boolean) -> Unit) {
        db.collection(FirestoreCollections.ADVERTISEMENTS.value)
            .document(advertisementID)
            .set(data, SetOptions.merge())
            .addOnSuccessListener {
                callback(true)
            }
            .addOnFailureListener {
                callback(false)
            }
    }
    fun updateupdateItemSoldedStatus(itemID: String): Task<Transaction> {
        val itemRef=db.collection(FirestoreCollections.ADVERTISEMENTS.value).document(itemID)
        return db.runTransaction {
            it.update(itemRef, mapOf("rated" to true))
        }
    }

    fun sellItem(advertisementID: String, userID: String): Task<Transaction> {
        val itemRef = db.collection(FirestoreCollections.ADVERTISEMENTS.value).document(advertisementID)
        val soldToUserRef = db.collection(FirestoreCollections.USERS_ITEMS_BOUGHT.value).document(userID)
        val interestedUserRef = db.collection(FirestoreCollections.USERS_ITEMS_OF_INTERESTS.value).document(userID)

        return db.runTransaction {
            it.update(itemRef, mapOf("likedPeopleList" to FieldValue.arrayRemove(userID), "sold" to true, "soldToUserId" to userID))
            it.set(soldToUserRef, mapOf(advertisementID to true), SetOptions.merge())
            it.set(interestedUserRef, mapOf(advertisementID to FieldValue.delete()), SetOptions.merge())
        }
    }

    fun uploadItemPicture(path: String, itemId: String, callback: (success: Boolean, imageURL: String?) -> Unit) {
        val ref = storageRef.child("pictures/items/{$itemId}.jpg")
        ref.putStream(FileInputStream(File(path)))
            .addOnSuccessListener {
                ref.downloadUrl
                    .addOnSuccessListener {
                        callback(true, it.toString())
                    }
                    .addOnFailureListener {
                        callback(false, null)
                    }
            }
            .addOnFailureListener {
                callback(false, null)
            }
    }

    fun getAdvertisementInterestedUsers(advertisementID: String, resultsCallback: (users: List<User>) -> Unit) {
        db.collection(FirestoreCollections.ADVERTISEMENTS.value)
            .document(advertisementID)
            .get()
            .addOnSuccessListener {
                if (it != null) {
                    val item = it.toObject(AdvertisementItem::class.java)
                    if (item?.likedPeopleList != null) {
                        UserRepository.getUsers(item.likedPeopleList) { usersList -> resultsCallback(usersList) }
                    } else {
                        resultsCallback(emptyList())
                    }
                } else {
                    resultsCallback(emptyList())
                }
            }
    }

    fun getPushID(): String {
        return FirebaseDatabase.getInstance().getReference(FirestoreCollections.ADVERTISEMENTS.value).push().key!!
    }
}
