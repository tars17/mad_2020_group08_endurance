package com.mad2020.group8.endurance.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mad2020.group8.endurance.data.User
import com.mad2020.group8.endurance.helper.Auth
import com.mad2020.group8.endurance.repository.UserRepository

class  UsersViewModel: ViewModel() {

    private val currentUser: MutableLiveData<User> = UserRepository.getByIdAndListen(Auth.getCurrentUserID()) as MutableLiveData<User>

    fun getCurrentUser(): LiveData<User> {
        return currentUser
    }

    fun getUser(userID: String): LiveData<User> {
        return UserRepository.getByIdAndListen(userID)
    }
}