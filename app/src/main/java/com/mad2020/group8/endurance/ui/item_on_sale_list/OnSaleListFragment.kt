package com.mad2020.group8.endurance.ui.item_on_sale_list

import android.os.Bundle
import android.view.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.adapter.ItemAdapter
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.data.ItemFilter
import com.mad2020.group8.endurance.viewmodel.OnSaleListViewModel
import kotlinx.android.synthetic.main.fragment_on_sale_list.*

class OnSaleListFragment : Fragment(), OnSaleItemsFiltersDialogFragment.OnSaleItemsFiltersDialogListener {
    private lateinit var vm: OnSaleListViewModel
    private lateinit var adapter: ItemAdapter
    private lateinit var onSaleItemsFiltersDialogFragment: OnSaleItemsFiltersDialogFragment

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        vm = ViewModelProvider(this).get(OnSaleListViewModel::class.java)
        return inflater.inflate(R.layout.fragment_on_sale_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = ItemAdapter(emptyList(), false)

        adapter.onItemClick = { item, position ->  onItemClick(item, position) }
        onSaleItemListRecycleView.layoutManager = LinearLayoutManager(context)
        onSaleItemListRecycleView.adapter = adapter

        vm.getItems().observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                adapter.setItems(it.sortedByDescending { item -> item.createdAt })
                onSaleEmptyListCard.visibility = View.GONE
            } else {
                onSaleEmptyListCard.visibility = View.VISIBLE
            }
        })

        vm.getFilter().observe(viewLifecycleOwner, Observer {
            onSaleItemsFiltersDialogFragment = OnSaleItemsFiltersDialogFragment(this, it)
        })

        // check if we clicked in a notification
        // TODO: check this more
//        val activityBundle = activity?.intent?.extras
//        if (activityBundle != null && activityBundle.getBoolean("isNotification")) {
//            if (!activityBundle.getString("itemID").isNullOrEmpty()) {
//                findNavController().navigate(R.id.action_nav_on_sale_list_to_item_details_fragment, bundleOf("itemID" to activityBundle.getString("itemID")))
//            }
//        }
    }

    private fun onItemClick(item: AdvertisementItem, position: Int) {
        findNavController().navigate(R.id.action_nav_on_sale_list_to_item_details_fragment, bundleOf("itemID" to item.id))
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.items_on_sale_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.filter_sale) {
            activity?.supportFragmentManager?.let { onSaleItemsFiltersDialogFragment.show(it, "OnSaleItemsFiltersDialogFragment") }
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onFilterClick(filter: ItemFilter) {
       if (filter.text.isNotEmpty() || filter.price > 0 || filter.category != "All") {
           vm.filterItems(filter) {}
       } else {
           vm.clearFilters()
       }
    }
}
