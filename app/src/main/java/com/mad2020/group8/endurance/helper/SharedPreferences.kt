package com.mad2020.group8.endurance.helper

import android.content.Context

object SharedPreferences {
    fun set(
        key: String,
        value: String,
        context: Context
    ) {
        val preferences = context.getSharedPreferences(key, Context.MODE_PRIVATE)
        with(preferences.edit()) {
            this.putString(key, value)
            commit()
        }
    }

    fun get(key: String, context: Context): String {
        val preferences = context.getSharedPreferences(key, Context.MODE_PRIVATE)
        return preferences.getString(key, "") ?: ""
    }
}