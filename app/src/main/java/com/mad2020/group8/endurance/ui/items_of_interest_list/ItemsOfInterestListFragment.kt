package com.mad2020.group8.endurance.ui.items_of_interest_list

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.adapter.ItemAdapter
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.viewmodel.ItemsOfInterestsListViewModel
import kotlinx.android.synthetic.main.fragment_items_of_interest_list.*

class ItemsOfInterestListFragment : Fragment() {
    private lateinit var adapter: ItemAdapter
    private lateinit var vm: ItemsOfInterestsListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        vm = ViewModelProvider(this).get(ItemsOfInterestsListViewModel::class.java)
        return inflater.inflate(R.layout.fragment_items_of_interest_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = ItemAdapter(emptyList(), false)

        adapter.onItemClick = { item, position ->  onItemClick(item, position) }
        itemsOfInterestListRecycleView.layoutManager = LinearLayoutManager(context)
        itemsOfInterestListRecycleView.adapter = adapter

        vm.getItems().observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                adapter.setItems(it.sortedByDescending { item -> item.createdAt })
                itemsOfInterestEmptyListCard.visibility = View.GONE
            } else {
                itemsOfInterestEmptyListCard.visibility = View.VISIBLE
            }
        })

        setUpItemTouchHelper()
    }

    private fun onItemClick(item: AdvertisementItem, position: Int) {
        findNavController().navigate(R.id.action_itemsOfInterestListFragment_to_item_details_fragment, bundleOf("itemID" to item.id))
    }

    private fun setUpItemTouchHelper() {
        val simpleItemTouchCallback: ItemTouchHelper.SimpleCallback =
            object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                // we want to cache these and not allocate anything repeatedly in the onChildDraw method
                var background: Drawable? = null
                var xMark: Drawable? = null
                var xMarkMargin = 0
                var initiated = false
                private fun init() {
                    background = ColorDrawable(Color.RED)
                    xMark = ContextCompat.getDrawable(context!!, R.drawable.ic_baseline_delete)
                    initiated = true
                }

                // not important, we don't want drag & drop
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }

                override fun getSwipeDirs(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder
                ): Int {
                    return super.getSwipeDirs(recyclerView, viewHolder)
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                    val item = adapter.getItemAtPosition(viewHolder.adapterPosition)
                    vm.removeItem(item.id)
                }

                override fun onChildDraw(
                    c: Canvas,
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    dX: Float,
                    dY: Float,
                    actionState: Int,
                    isCurrentlyActive: Boolean
                ) {
                    val itemView = viewHolder.itemView

                    // not sure why, but this method get's called for viewholder that are already swiped away
                    if (viewHolder.adapterPosition == -1) {
                        // not interested in those
                        return
                    }
                    if (!initiated) {
                        init()
                    }

                    // draw red background
                    background!!.setBounds(
                        itemView.right + dX.toInt(),
                        itemView.top,
                        itemView.right,
                        itemView.bottom
                    )
                    background!!.draw(c)

                    // draw x mark
                    val itemHeight = itemView.bottom - itemView.top
                    val intrinsicWidth = xMark!!.intrinsicWidth
                    val intrinsicHeight = xMark!!.intrinsicWidth
                    val xMarkLeft = itemView.right - xMarkMargin - intrinsicWidth
                    val xMarkRight = itemView.right - xMarkMargin
                    val xMarkTop = itemView.top + (itemHeight - intrinsicHeight) / 2
                    val xMarkBottom = xMarkTop + intrinsicHeight
                    xMark!!.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom)
                    xMark!!.draw(c)
                    super.onChildDraw(
                        c,
                        recyclerView,
                        viewHolder,
                        dX,
                        dY,
                        actionState,
                        isCurrentlyActive
                    )
                }
            }

        val mItemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        mItemTouchHelper.attachToRecyclerView(itemsOfInterestListRecycleView)
    }

}