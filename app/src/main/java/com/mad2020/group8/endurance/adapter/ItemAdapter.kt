package com.mad2020.group8.endurance.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.squareup.picasso.Picasso
import java.io.File
import java.text.DecimalFormat

class ItemAdapter(private var items: List<AdvertisementItem>, private var isOwnedByCurrentUser: Boolean = true): RecyclerView.Adapter<ItemAdapter.ViewHolder>() {
    var onItemClick: ((item: AdvertisementItem, position: Int) -> Unit)? = null
    var onItemEditClick: ((item: AdvertisementItem, position: Int) -> Unit)? = null

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    fun getItemAtPosition(position: Int): AdvertisementItem {
        return items[position]
    }

    inner class ViewHolder(v: View): RecyclerView.ViewHolder(v) {
        var id: String = ""
        val photo = v.findViewById<ImageView>(R.id.itemPhotoImageView)
        val title = v.findViewById<TextView>(R.id.titleTextView)
        val location = v.findViewById<TextView>(R.id.locationTextView)
        val price = v.findViewById<Chip>(R.id.priceChip)
        val category = v.findViewById<TextView>(R.id.categoryTextView)
        val editButton = v.findViewById<Button>(R.id.editButton)
        val notInSaleText = v.findViewById<TextView>(R.id.notInSaleText)

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(items[adapterPosition], adapterPosition)
            }

            editButton.setOnClickListener {
                onItemEditClick?.invoke(items[adapterPosition], adapterPosition)
            }
        }

        fun bind(item: AdvertisementItem) {
            id = item.id
            title.text = item.title
            location.text = item.address
            price.text =  DecimalFormat("#.##").format(item.price).toString() + "$"
            category.text = item.category

            if (!item.photo.isNullOrEmpty()) {
                try {
                    if (item.photo!!.startsWith("http")) {
                        Picasso.get().load(item.photo).placeholder(R.drawable.item_placeholder).error(R.drawable.item_placeholder).into(photo)
                    } else {
                        Picasso.get().load(File(item.photo!!)).placeholder(R.drawable.item_placeholder).error(R.drawable.item_placeholder).into(photo)
                    }
                } catch (e: Exception) {
                    Log.e("ENDURANCE", "Couldn't set the user picture. Err=$e")
                }
            }

            if (!isOwnedByCurrentUser) {
                editButton.visibility = View.GONE
                // TODO: scale this using display metrics
                location.setPadding(0, 0, 0, 35)
            }

            if (isOwnedByCurrentUser && item.blocked) {
                notInSaleText.visibility = View.VISIBLE
            }
        }
    }

    fun setItems(items: List<AdvertisementItem>) {
        val oldList = this.items
        val diffResult = DiffUtil.calculateDiff(ItemDiffCallback(
            oldList,
            items
        ))

        this.items = items
        diffResult.dispatchUpdatesTo(this)
    }

    class ItemDiffCallback(
        var oldList: List<AdvertisementItem>,
        var newList: List<AdvertisementItem>
    ): DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].equals(newList[newItemPosition])
        }

    }
}