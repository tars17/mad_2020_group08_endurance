package com.mad2020.group8.endurance.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mad2020.group8.endurance.data.User
import com.mad2020.group8.endurance.repository.UserRepository

class LikedPeopleListViewModel(userIDs: List<String>) : ViewModel() {

    private val users: MutableLiveData<List<User>> = MutableLiveData()

    init {
        UserRepository.getUsers(userIDs) {
            users.value = it
        }
    }

    fun getUsers(): LiveData<List<User>> {
        return users
    }
}