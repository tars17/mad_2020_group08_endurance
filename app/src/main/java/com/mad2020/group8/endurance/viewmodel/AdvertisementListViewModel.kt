package com.mad2020.group8.endurance.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.repository.AdvertisementRepository

class AdvertisementListViewModel: ViewModel() {
    private val items: MutableLiveData<List<AdvertisementItem>> = MutableLiveData()

    init {
        loadItems()
    }

    private fun loadItems() {
        AdvertisementRepository.getItems()
            .addSnapshotListener { snap, err ->
                if (err != null) {
                    Log.d("Error", err.toString())
                }

                if (snap != null ) {
                    val itemsList: ArrayList<AdvertisementItem> = ArrayList()
                    for (item in snap.documents) {
                        val i = item.toObject(AdvertisementItem::class.java)?.apply { id = item.id }
                        if (i != null && !i.sold) {
                            itemsList.add(i)
                        }
                    }

                    items.value = itemsList
                }
            }
    }

    fun getItems(): LiveData<List<AdvertisementItem>> {
       return items
    }
}