package com.mad2020.group8.endurance.helper

object Category {
    private var map: LinkedHashMap<String, Array<String>> = linkedMapOf(
        "Arts & Crafts" to arrayOf("Painting, Drawing & Art Supplies", "Sewing", "Scrapbooking & Stamping", "Party Decorations & Supplies"),
        "Sports & Hobby" to arrayOf("Sports and Outdoors", "Outdoor Recreation", "Sports & Fitness", "Pet Supplies"),
        "Baby" to arrayOf("Apparel & Accessories", "Baby & Toddler Toys", "Car Seats & Accessories", "Pregnancy & Maternity", "Strollers & Accessories"),
        "Women's fashion" to arrayOf("Clothing", "Shoes", "Watches", "Handbags", "Accessories"),
        "Men's fashion" to arrayOf("Clothing", "Shoes", "Watches", "Accessories"),
        "Electronics" to arrayOf("Computers", "Monitors", "Printers & Scanners", "Camera & Photo", "Smartphone & Tablet", "Audio", "Television & Video", "Video Game Consoles", "Wearable Technology", "Accessories & Supplies", "Irons & Steamers", "Vacuums & Floor Care"),
        "Games & Videogames" to arrayOf("Action Figures & Statues", "Arts & Crafts", "Building Toys", "Dolls & Accessories", "Kids' Electronics", "Learning & Education", "Tricycles, Scooters & Wagons", "Videogames"),
        "Automotive" to arrayOf("Car Electronics & Accessories", "Accessories", "Motorcycle & Powersports", "Replacement Parts", "RV Parts & Accessories", "Tools & Equipment")
    )

    fun getCategories(): List<String> {
        return map.map { it.key }
    }

    fun getSubcategories(category: String): Array<String> {
        return map[category] ?: emptyArray()
    }
}