package com.mad2020.group8.endurance.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.Transaction
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.helper.Auth

object InterestedItemsRepository {
    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()

    fun getItemsOfInterest(): MutableLiveData<List<AdvertisementItem>> {
        val currentUserID = Auth.getCurrentUserID()
        val data = MutableLiveData<List<AdvertisementItem>>()

        db.collection(FirestoreCollections.USERS_ITEMS_OF_INTERESTS.value)
            .document(currentUserID)
            .addSnapshotListener { snap, err ->
                if (err != null) {
                    Log.d("Error", err.toString())
                }

                if (snap != null ) {
                    val keys = snap.data?.keys?.toMutableList() ?: emptyList<String>()
                    if (keys.isNotEmpty()) {
                        db.collection(FirestoreCollections.ADVERTISEMENTS.value)
                            .whereIn("id", keys)
                            .addSnapshotListener { itemsSnap, itemsErr ->
                                if (itemsErr != null) {
                                    Log.d("Error", itemsErr.toString())
                                }

                                val itemsList: ArrayList<AdvertisementItem> = ArrayList()
                                if (itemsSnap != null ) {
                                    for (item in itemsSnap.documents) {
                                        val i = item.toObject(AdvertisementItem::class.java)?.apply { id = item.id }
                                        if (i != null) {
                                            itemsList.add(i)
                                        }
                                    }
                                }

                                data.value = itemsList
                            }
                    } else {
                        data.value = emptyList()
                    }
                } else {
                    data.value = emptyList()
                }
            }

        return data
    }

    fun addItemToInterests(itemID: String): Task<Void> {
        return db.collection(FirestoreCollections.USERS_ITEMS_OF_INTERESTS.value)
            .document(Auth.getCurrentUserID())
            .set(mapOf(itemID to true), SetOptions.merge())
    }

    fun removeItemFromInterests(itemID: String): Task<Transaction> {
        val interestsRef = db.collection(FirestoreCollections.USERS_ITEMS_OF_INTERESTS.value).document(Auth.getCurrentUserID())
        val itemRef = db.collection(FirestoreCollections.ADVERTISEMENTS.value).document(itemID)

        return db.runTransaction {
            it.set(interestsRef, mapOf(itemID to FieldValue.delete()), SetOptions.merge())
            it.update(itemRef, mapOf("likedPeopleList" to FieldValue.arrayRemove(Auth.getCurrentUserID())))
        }
    }
}
