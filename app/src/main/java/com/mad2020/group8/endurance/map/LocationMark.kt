package com.mad2020.group8.endurance.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.mad2020.group8.endurance.MainActivity
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import com.mad2020.group8.endurance.R

class LocationMark {
    private var googleMap: GoogleMap? = null

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private val locationPermission = Manifest.permission.ACCESS_FINE_LOCATION




        @SuppressLint("MissingPermission")
        public fun handleLocation(location: Location) {
            val map = googleMap ?: return
            map?.isMyLocationEnabled = true
            map?.uiSettings?.isMyLocationButtonEnabled = true
            val latLng = LatLng(location.latitude, location.longitude)

            map?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
            map?.addMarker(MarkerOptions().position(latLng).title("Marker"));
//        map?.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            //定位成功后可停止获取位置更新
            Log.d("Location???", latLng.toString())
            stopLocationUpdates()
        }

        public fun stopLocationUpdates() {
            fusedLocationClient.removeLocationUpdates(locationCallback)
        }


    }