package com.mad2020.group8.endurance.map

import android.app.Application
import android.location.Location
import android.util.Log

class LocationApplication : Application() {
    companion object {
        private var Location: Location? = null
    }

    fun getLocation(): Location? {
        Log.d("start get Location1", Location.toString())
        return Location
    }

    fun setLocation(location: Location) {
        Location = location

    }
}