package com.mad2020.group8.endurance.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mad2020.group8.endurance.data.User
import com.mad2020.group8.endurance.repository.UserRepository

class CommentListViewModel: ViewModel() {
    fun getUser(userID: String): LiveData<User> {
        return UserRepository.getByIdAndListen(userID)
    }
}