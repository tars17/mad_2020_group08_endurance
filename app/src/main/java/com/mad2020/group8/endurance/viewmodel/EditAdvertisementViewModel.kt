package com.mad2020.group8.endurance.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.Transaction
import com.google.firebase.storage.FirebaseStorage
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.data.User
import com.mad2020.group8.endurance.repository.AdvertisementRepository

class EditAdvertisementViewModel(private val advertisementID: String?, private val isFromListPage: Boolean): ViewModel() {
    private var advertisement: MutableLiveData<AdvertisementItem> = MutableLiveData()
    private var advertisementInterestedUsers: MutableLiveData<List<User>> = MutableLiveData()
    private var advertisementPicture: MutableLiveData<String> = MutableLiveData()
    private var storageRef = FirebaseStorage.getInstance().reference
    private var isSaving: Boolean = false

    init {
        if (!advertisementID.isNullOrEmpty()) {
            advertisement = AdvertisementRepository.getItem(advertisementID) as MutableLiveData<AdvertisementItem>
        }
    }

    fun getAdvertisement(): LiveData<AdvertisementItem> {
        return advertisement
    }

    fun setLocation(location:String) {
        val updatedUser = advertisement.value
        updatedUser?.location = location
        advertisement.value = updatedUser
    }

    fun setAddress(address:String) {
        val updatedUser = advertisement.value
        updatedUser?.address = address
        advertisement.value = updatedUser
    }

    fun getAdvertisementPicture(): MutableLiveData<String> {
        return advertisementPicture
    }

    fun setAdvertisementPicture(picture: String) {
        advertisementPicture.value = picture
    }

    fun setOnlineAdvertisementPicture(itemID: String) {
        val updatedAdvertisement = advertisement.value
        val urlPath = "pictures/items/{$itemID}.jpg"
        val userRef = storageRef.child(urlPath)

        userRef.downloadUrl.addOnSuccessListener {
            updatedAdvertisement?.photo = it.toString()
            updatedAdvertisement?.updatedAt = System.currentTimeMillis() / 1000
            advertisement.value = updatedAdvertisement
        }
    }

    fun addAdvertisement(advertisementItem: AdvertisementItem, callback: (success: Boolean) -> Unit) {
        setIsSaving(true)
        if (!advertisementItem.photo.isNullOrEmpty()) {
            AdvertisementRepository.uploadItemPicture(advertisementItem.photo!!, advertisementItem.id) { success, imageURL ->
                if (success) {
                    advertisementItem.photo = imageURL
                }

                AdvertisementRepository.addItem(advertisementItem, callback)
            }
        } else {
            AdvertisementRepository.addItem(advertisementItem, callback)
        }
    }

    fun updateAdvertisement(updates: MutableMap<String, Any?>, callback: (success: Boolean) -> Unit) {
        setIsSaving(true)
        if (updates["photo"] != null && !(updates["photo"] as String).startsWith("http")) {
            AdvertisementRepository.uploadItemPicture(updates["photo"] as String, this.advertisementID as String) { success, imageURL ->
                if (success) {
                    updates["photo"] = imageURL!!
                }

                AdvertisementRepository.updateItem(this.advertisementID, updates, callback)
            }
        } else {
            AdvertisementRepository.updateItem(this.advertisementID as String, updates, callback)
        }
    }

    fun updatePicture(itemID: String) {
        advertisement.value?.let {
            AdvertisementRepository.updateItem(itemID, mapOf("photo" to it.photo)){}
        }
    }

    fun getIsFromListPage(): Boolean {
        return isFromListPage
    }

    fun getAdvertisementId(): String? {
        return advertisementID
    }

    fun getAdvertisementPushID(): String {
       return AdvertisementRepository.getPushID()
    }

    fun sellItem(toUserID: String): Task<Transaction> {
        return AdvertisementRepository.sellItem(advertisementID!!, toUserID)
    }

    fun loadAdvertisementInterestedUsers(advertisementID: String) {
        AdvertisementRepository.getAdvertisementInterestedUsers(advertisementID) {
            advertisementInterestedUsers.value = it
        }
    }

    fun clearAdvertisementInterestedUser() {
        advertisementInterestedUsers.value = emptyList()
    }

    fun getAdvertisementInterestedUsers(): MutableLiveData<List<User>> {
        return advertisementInterestedUsers
    }

    fun getIsSaving(): Boolean {
        return isSaving
    }

    fun setIsSaving(status: Boolean) {
        this.isSaving = status
    }

    fun setTitle(value: String) {
        val currentItem = advertisement.value
        if (currentItem != null) {
            currentItem.title = value
            advertisement.value = currentItem
        }
    }
}