package com.mad2020.group8.endurance.adapter

import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mad2020.group8.endurance.R


class CommentAdapter(private var commentList : ArrayList<String>): RecyclerView.Adapter<CommentAdapter.ViewHolder>() {
    override fun getItemCount()=commentList.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.comment_list_single_comment, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: CommentAdapter.ViewHolder, position: Int) {
        holder.bind( commentList[position])
    }
    inner class ViewHolder(v: View): RecyclerView.ViewHolder(v) {
        var commentContent = v.findViewById<TextView>(R.id.commentContent)


        fun bind (comment: String) {
            commentContent.text = comment
            //commentContent.movementMethod=ScrollingMovementMethod()
        }
    }
    fun setComment(commentList: ArrayList<String>) {
        val oldList = this.commentList
        val diffResult = DiffUtil.calculateDiff(
            CommentAdapter.CommentsDiffCallback(
                oldList,
                commentList
            )
        )

        this.commentList = commentList
        diffResult.dispatchUpdatesTo(this)
    }

    class CommentsDiffCallback(
        var oldList: ArrayList<String>,
        var newList: ArrayList<String>
    ): DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].equals(newList[newItemPosition])
        }

    }

}