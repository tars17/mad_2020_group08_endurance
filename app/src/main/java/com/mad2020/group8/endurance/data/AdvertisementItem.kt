package com.mad2020.group8.endurance.data

data class AdvertisementItem(
    var id: String = "",
    var title: String = "",
    var description: String = "",
    var price: Double = 0.0,
    var category: String = "",
    var subcategory: String = "",
    var location: String = "",
    var address: String = "",
    var expirationDate: String = "",
    var photo: String? = "",
    val userId: String = "",
    var sold: Boolean = false,
    var blocked: Boolean = false,
    val createdAt: Long = System.currentTimeMillis() / 1000, // unix time
    var updatedAt: Long = System.currentTimeMillis() / 1000, // unix time
    val likedPeopleList: ArrayList<String>? =  arrayListOf(),
    val soldToUserId: String? = "",
    val rated: Boolean = false
) {
    override fun equals(other: Any?): Boolean {
        if (javaClass != other?.javaClass) {
            return false
        }

        other as AdvertisementItem

        if (id != other.id) {
            return false
        }

        if (title != other.title) {
            return false
        }

        if (description != other.description) {
            return false
        }

        if (price != other.price) {
            return false
        }

        if (category != other.category) {
            return false
        }

        if (subcategory != other.subcategory) {
            return false
        }

        if (location != other.location) {
            return false
        }

        if (address != other.address) {
            return false
        }

        if (expirationDate != other.expirationDate) {
            return false
        }

        if (photo != other.photo) {
            return false
        }

        if (userId != other.userId) {
            return false
        }

        if (createdAt != other.createdAt) {
            return false
        }

        if (likedPeopleList?.size != other.likedPeopleList?.size) {
            return false
        }

        if (!likedPeopleList?.toArray()?.contentEquals(other.likedPeopleList?.toArray()!!)!!) {
            return false
        }

        if (blocked != other.blocked) {
            return false
        }

        if (sold != other.sold) {
            return false
        }

        if (updatedAt != other.updatedAt) {
            return false
        }

        if (soldToUserId != other.soldToUserId) {
            return false
        }

        if (rated != other.rated) {
            return false
        }

        return true
    }
}
