package com.mad2020.group8.endurance.helper

import android.content.Context
import android.util.Log
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.FragmentActivity
import java.lang.Exception

object Keyboard {
    fun hide(activity: FragmentActivity) {
        // close the keyboard if its open
        try {
            val inputManager: InputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            val currentFocusedView = activity.currentFocus
            if (currentFocusedView !== null) {
                inputManager.hideSoftInputFromWindow(currentFocusedView.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            }
        } catch (e: Exception) {
            Log.e("Error", e.stackTrace.toString())
        }
    }
}