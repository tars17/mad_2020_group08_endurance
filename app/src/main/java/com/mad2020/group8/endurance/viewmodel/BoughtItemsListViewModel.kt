package com.mad2020.group8.endurance.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.repository.BoughtItemsRepository

class BoughtItemsListViewModel : ViewModel() {

    private val items: MutableLiveData<List<AdvertisementItem>> = BoughtItemsRepository.getItemsBought()

    fun getItems(): MutableLiveData<List<AdvertisementItem>> {
        return items
    }
}
