package com.mad2020.group8.endurance.ui.item_details

import android.Manifest
import android.annotation.SuppressLint
import android.app.DownloadManager
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.beust.klaxon.*
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.snackbar.Snackbar
import com.mad2020.group8.endurance.GetLocationService
import com.mad2020.group8.endurance.MainActivity
import com.mad2020.group8.endurance.R
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.map.LocationApplication
import com.mad2020.group8.endurance.viewmodel.AdvertisementDetailsViewModel
import com.mad2020.group8.endurance.viewmodel.AdvertisementDetailsViewModelFactory
import com.mad2020.group8.endurance.viewmodel.EditAdvertisementViewModel
import com.mad2020.group8.endurance.viewmodel.EditAdvertisementViewModelFactory
import java.net.URL
import java.util.*
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread


class routeToSeller : Fragment() {
    private lateinit var vm: EditAdvertisementViewModel
    private lateinit var vmFactory: EditAdvertisementViewModelFactory
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private val PERMISSIONS_LOCATION = 3
    private var latLng = LatLng(39.937795, 116.387224)

    private var googleMap: GoogleMap? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as MainActivity).supportActionBar?.title = "Edit profile"
        setHasOptionsMenu(true)
        val itemID = arguments?.get("itemID") as String?
        vmFactory = EditAdvertisementViewModelFactory(
            itemID,
            arguments?.getBoolean("fromItemsList") as Boolean
        )
        vm = ViewModelProvider(this, vmFactory).get(EditAdvertisementViewModel::class.java)
        return inflater.inflate(R.layout.fragment_google_map_item, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).supportActionBar?.title = "Route to seller"

        fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(activity as MainActivity)

        locationRequest = LocationRequest().apply {
            interval = 5000  //请求时间间隔
            fastestInterval = 3000 //最快时间间隔
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                Log.d("Location???", "callback")
                locationResult ?: return
                handleLocation(locationResult.lastLocation)
            }
        }


        val mapFrag = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        vm.getAdvertisement().observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (!it.location.isNullOrEmpty()) {
                try {
                    val userLocation = it.location.split(",".toRegex()).toTypedArray()
                    latLng = LatLng(userLocation[0].toDouble(), userLocation[1].toDouble())
                } catch (e: Exception) {
                    Log.e("Warning", "Wrong location format!")
                }
                mapFrag.getMapAsync {
                    googleMap = it
                    requestLocationPermission()
                }
            }
        })
        requestLocationPermission()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

//        findNavController().navigate(
//            R.id.action_map_to_item_details_fragment,
//            bundleOf("itemID" to arguments?.getString("itemID"))
//        )

        findNavController().popBackStack()

        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        if (PERMISSIONS_LOCATION == requestCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val serviceIntend = Intent(activity, GetLocationService::class.java)
                activity?.stopService(serviceIntend)
                activity?.startService(serviceIntend)
                requestLocationUpdate()
            }
        } else {
            Toast.makeText(activity, "Permission denied", Toast.LENGTH_SHORT).show()
//            findNavController().navigate(
//                R.id.action_map_to_item_details_fragment,
//                bundleOf("itemID" to arguments?.getString("itemID"))
//            )
            findNavController().popBackStack()
        }
    }

    @SuppressLint("WrongConstant")
    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestLocationPermission() {
        when (PERMISSIONS_LOCATION) {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) -> requestLocationUpdate()
            else -> {
                val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
                requestPermissions(permissions, PERMISSIONS_LOCATION)
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestLocationUpdate() {
        Log.d("Location???new", LocationApplication().getLocation().toString())
        if (LocationApplication().getLocation() == null) {
            Log.d("Location???new", LocationApplication().getLocation().toString())
            Toast.makeText(activity, "Can't find your location, please try again!", Toast.LENGTH_SHORT).show()
//            findNavController().navigate(
//                R.id.action_map_to_item_details_fragment,
//                bundleOf("itemID" to arguments?.getString("itemID"))
//            )
            findNavController().popBackStack()
            return
        }
           handleLocation(LocationApplication().getLocation()!!)

    }

    @SuppressLint("MissingPermission")
    private fun handleLocation(location: Location) {
        Log.d("Location???", "handle")

        val map = googleMap ?: return
        map?.isMyLocationEnabled = true
        map?.uiSettings?.isMyLocationButtonEnabled = true
        val myLocation = LatLng(location.latitude, location.longitude)
//        map?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15f))
        map?.addMarker(
            MarkerOptions().position(myLocation)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
        );
        map?.addMarker(
            MarkerOptions().position(latLng).title("Seller")
        );
        val options = PolylineOptions()
        options.color(Color.RED)
        options.width(5f)
        val url = getURL(myLocation, latLng)
        Log.d("url:", url)
        async {
            // Connect to URL, download content and convert into string asynchronously
            val result = URL(url).readText()
            uiThread {
                val parser: Parser = Parser()
                val stringBuilder: StringBuilder = StringBuilder(result)
                val json: JsonObject = parser.parse(stringBuilder) as JsonObject
                val routes = json.array<JsonObject>("routes")
                if (!routes!!.value.isNullOrEmpty()) {

                    val points = routes!!["legs"]["steps"][0] as JsonArray<JsonObject>

                    val polypts = points.flatMap {
                        decodePoly(it.obj("polyline")?.string("points")!!)
                    }
                    val LatLongB = LatLngBounds.Builder()
                    options.add(myLocation)
                    LatLongB.include(myLocation)
                    for (point in polypts) options.add(point)
                    options.add(latLng)
                    LatLongB.include(latLng)
                    val bounds = LatLongB.build()
                    map!!.addPolyline(options)
                    map!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
                } else {
                    try {
                        Toast.makeText(
                            activity,
                            "Seller too far away from you!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } catch (e: Exception) {

                    }
                }
            }
        }
        stopLocationUpdates()
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    private fun getURL(from: LatLng, to: LatLng): String {
        val origin = "origin=" + from.latitude + "," + from.longitude
        val dest = "destination=" + to.latitude + "," + to.longitude
        val sensor = "sensor=false"
        val key = "AIzaSyCpZoLJMl3kNekCZMnXpr5KbEDVcz-Vpyc"
        val params = "$origin&$dest&$sensor&key=$key"
        return "https://maps.googleapis.com/maps/api/directions/json?$params"
    }

    private fun decodePoly(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val p = LatLng(
                lat.toDouble() / 1E5,
                lng.toDouble() / 1E5
            )
            poly.add(p)
        }

        return poly
    }

}