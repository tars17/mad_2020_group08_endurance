package com.mad2020.group8.endurance

import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.content.IntentSender
import android.os.IBinder
import android.os.Looper
import android.util.Log
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.mad2020.group8.endurance.map.LocationApplication

/**
 * Created by luiz on 01/03/18.
 */

class GetLocationService : Service() {

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    lateinit var settingsClient: SettingsClient

    lateinit var locationRequest: LocationRequest

    lateinit var locationCallback: LocationCallback
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return Service.START_STICKY
    }

    override fun onCreate() {
        super.onCreate()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        createLocationRequest()

        createLocationCallBack()

    }


    fun createLocationRequest() {
        locationRequest = LocationRequest().apply {
            interval = 5000
            fastestInterval = 5000

            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)

        settingsClient = LocationServices.getSettingsClient(this)

        val task: Task<LocationSettingsResponse> =
            settingsClient.checkLocationSettings(builder.build())
        this.task(task)
    }

    @SuppressLint("MissingPermission")
    fun task(task:Task<LocationSettingsResponse>){
        task.addOnSuccessListener { locationSettingsResponse ->
            // All location settings are satisfied. The client can initialize
            // location requests here.
            // ...
            Log.d("test ","waiting for locationCallback")
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.getMainLooper()
            )

        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    //exception.startResolutionForResult(this@MainActivity,
                    //REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }

    fun createLocationCallBack(){

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                Log.d("start get Location2", "12${locationResult.toString()}")

                LocationApplication().setLocation(locationResult.lastLocation)

                //Do what you want with the position here

            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("Myservice", "onDestory")
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

}