package com.mad2020.group8.endurance.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class EditAdvertisementViewModelFactory(private val advertisementID: String?, private val isFromListPage: Boolean) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EditAdvertisementViewModel::class.java)) {
            return EditAdvertisementViewModel(advertisementID, isFromListPage) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}