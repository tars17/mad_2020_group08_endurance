package com.mad2020.group8.endurance.helper

import android.graphics.Bitmap
import android.util.Log
import java.io.FileOutputStream
import java.io.IOException

object File {
    fun bitmapToFile(bitmap: Bitmap, filePath: String): Boolean {
        return try {
            val out = FileOutputStream(filePath)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
            out.flush()
            out.close()
            true
        } catch (e: IOException) {
            Log.d("FileHelper", "Error while saving bitmap to file")
            Log.d("FileHelper", e.printStackTrace().toString())
            false
        }
    }
}