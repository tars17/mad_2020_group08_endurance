package com.mad2020.group8.endurance.repository

enum class FirestoreCollections(val value: String) {
    USERS("users"),
    ADVERTISEMENTS("advertisements"),
    USERS_ITEMS_BOUGHT("users_items_bought"),
    USERS_ITEMS_OF_INTERESTS("users_items_of_interests")
}
