package com.mad2020.group8.endurance.repository

import android.util.Log
import com.algolia.search.saas.Client
import com.algolia.search.saas.Query
import com.google.gson.Gson
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.data.ItemFilter
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception

object AlgoliaRepository {
    private val index = "ITEMS"
    private val client = Client("JXINXIP2GS", "b69a7f314e5af9aa2d1a00eae172b9fe")

    fun search(filter: ItemFilter, callback: (data: JSONObject?) -> Unit) {
        var searchQuery = ""
        if (filter.text.isNotEmpty()) {
            searchQuery += filter.text
        }

        if (filter.category.isNotEmpty() && filter.category != "All") {
            searchQuery = "$searchQuery ${filter.category}"
        }

        val query = Query(searchQuery)
        if (filter.price > 0) {
            query.filters = "price <= ${filter.price}"
        }

        client.getIndex(index).searchAsync(query) { data, error ->
            if (error != null) {
                callback(null)
            } else {
                callback(data)
            }
        }
    }

    fun fromSearchResponseToItem(data: JSONObject): AdvertisementItem? {
        var likedPeopleList = arrayListOf<String>()
        try {
            likedPeopleList = Gson().fromJson(
                data.getJSONArray("likedPeopleList").toString(), ArrayList<String>()::class.java
            )
        } catch (e: Exception) {
        }

        try {
            return AdvertisementItem(
                data["id"] as String,
                data["title"] as String,
                data["description"] as String,
                (data["price"] as Int).toDouble(),
                data["category"] as String,
                data["subcategory"] as String,
                data["location"] as String,
                data.optString("address"),
                data["expirationDate"] as String,
                data.optString("photo"),
                data["userId"] as String,
                data.optBoolean("sold"),
                data.optBoolean("blocked"),
                (data["createdAt"] as Int).toLong(),
                data.optInt("updatedAt").toLong(),
                likedPeopleList,
                data.optString("soldToUserId")
            )
        } catch (ex: Exception) {
            Log.d("AlgoliaRepository", "Error during deserialization")
            Log.d("AlgoliaRepository", ex.toString())
            return null
        }

    }
}
