package com.mad2020.group8.endurance.data

data class ItemFilter(
    val text: String,
    val price: Int,
    val category: String
)
