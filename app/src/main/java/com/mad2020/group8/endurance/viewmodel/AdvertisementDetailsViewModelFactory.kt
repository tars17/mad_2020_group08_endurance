package com.mad2020.group8.endurance.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class  AdvertisementDetailsViewModelFactory(private val itemID: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AdvertisementDetailsViewModel::class.java)) {
            return AdvertisementDetailsViewModel(itemID) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}