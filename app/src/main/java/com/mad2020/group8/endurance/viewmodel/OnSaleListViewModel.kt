package com.mad2020.group8.endurance.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.QuerySnapshot
import com.mad2020.group8.endurance.data.AdvertisementItem
import com.mad2020.group8.endurance.data.ItemFilter
import com.mad2020.group8.endurance.helper.Auth
import com.mad2020.group8.endurance.repository.AdvertisementRepository
import com.mad2020.group8.endurance.repository.AlgoliaRepository
import org.json.JSONArray

class OnSaleListViewModel : ViewModel() {

    private val items: MutableLiveData<List<AdvertisementItem>> = MutableLiveData()
    private var filter: MutableLiveData<ItemFilter> = MutableLiveData(ItemFilter("", 0, "All"))

    init {
        loadItemsOnSale()
    }

    private fun processItemsResponse(snap: QuerySnapshot?, err: FirebaseFirestoreException?) {
        if (err != null) {
            Log.d("Error", err.toString())
        }

        val itemsList: ArrayList<AdvertisementItem> = ArrayList()
        if (snap != null ) {
            for (item in snap.documents) {
                val i = item.toObject(AdvertisementItem::class.java)?.apply { id = item.id }
                if (i != null && i.userId != Auth.getCurrentUserID() && !i.blocked && !i.sold) {
                    itemsList.add(i)
                }
            }
        }

        items.value = itemsList
    }

    private fun loadItemsOnSale() {
        AdvertisementRepository.getItemsOnSale()
            .addSnapshotListener { snap, err -> processItemsResponse(snap, err) }
    }

    fun getItems(): LiveData<List<AdvertisementItem>> {
        return items
    }

    fun getFilter(): MutableLiveData<ItemFilter> {
        return filter
    }

    fun getItem(itemID: String): AdvertisementItem? {
        return items.value!!.find {
            it.id == itemID
        }
    }

    fun filterItems(filter: ItemFilter, callback: () -> Unit) {
        this.filter.value = filter

        AlgoliaRepository.search(filter) {
            Log.d("SearchContent", it.toString())
            if (it != null) {
                if (it["nbHits"] as Int > 0) {
                    Log.d("SearchContentHits", it["nbHits"].toString())
                    val hits = it["hits"] as JSONArray
                    val results = mutableListOf<AdvertisementItem>()
                    for (i in 0 until hits.length()) {
                        val result = AlgoliaRepository.fromSearchResponseToItem(hits.getJSONObject(i))
                        if (result != null) {
                            results.add(result)
                        }
                    }

                    items.value = results
                    callback()
                } else {
                    items.value = emptyList()
                    callback()
                }
            }
        }
    }

    fun clearFilters() {
        // fetch items only if the previous filters had any values
        if (filter.value?.text?.isNotEmpty()!! || filter.value?.price!! > 0 || filter.value?.category!! != "All") {
            filter.value = ItemFilter("", 0, "All")
            loadItemsOnSale()
        }
    }
}
