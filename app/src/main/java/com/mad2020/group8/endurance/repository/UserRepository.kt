package com.mad2020.group8.endurance.repository

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.mad2020.group8.endurance.data.User
import com.mad2020.group8.endurance.helper.Auth
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream

object UserRepository {
    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()
    private var storageRef = FirebaseStorage.getInstance().reference

    fun getById(userId: String): LiveData<User> {
        val data = MutableLiveData<User>()
        db.collection(FirestoreCollections.USERS.value)
            .document(userId)
            .get()
            .addOnSuccessListener {
                data.value = it.toObject(User::class.java)?.apply { id = it.id }
            }

        return data
    }

    fun getUsers(userIds: List<String>, resultsCallback: (users: List<User>) -> Unit) {
        db.collection(FirestoreCollections.USERS.value)
            .get()
            .addOnSuccessListener {
                val usersList: ArrayList<User> = ArrayList()
                for (user in it.documents) {
                   if(user.id in userIds)
                   {
                       val u = user.toObject(User::class.java)?.apply { id = user.id }
                       if (u != null) {
                           usersList.add(u)
                       }
                   }
                }

                resultsCallback(usersList)
            }
            .addOnFailureListener {
                resultsCallback(emptyList())
            }
    }

    fun updateUser(user: User, callback: (success: Boolean) -> Unit) {
        val userRef = db.collection(FirestoreCollections.USERS.value).document(user.id)

        if (!user.picture.isNullOrEmpty() && !user.picture!!.startsWith("http")) {
            uploadUserPicture(user.picture!!) { success, imageURL ->
                if (success) {
                    user.picture = imageURL
                }

                userRef.set(user)
                    .addOnSuccessListener {
                        callback(true)
                    }
                    .addOnFailureListener {
                        callback(false)
                    }
            }
        } else {
            userRef.set(user)
                .addOnSuccessListener {
                    callback(true)
                }
                .addOnFailureListener {
                    callback(false)
                }
        }
    }

    fun getByIdAndListen(userId: String): LiveData<User> {
        val data = MutableLiveData<User>()

        db.collection(FirestoreCollections.USERS.value)
            .document(userId)
            .addSnapshotListener { snap, error ->
                if (error != null) {
                    Log.d("Error", error.toString())
                }

                data.value = snap?.toObject(User::class.java).apply {
                    this?.id = snap?.id.toString()
                }
            }

        return data
    }

    //  TODO: make this use cursors to return not the whole list
    fun getAll(): LiveData<List<User>> {
        val usersData = MutableLiveData<List<User>>()

        db.collection(FirestoreCollections.USERS.value)
            .get()
            .addOnSuccessListener {
                val tempList: ArrayList<User> = ArrayList()
                for (user in it.documents) {
                    val u = user.toObject(User::class.java)?.apply { id = user.id }
                    if (u != null) {
                        tempList.add(u)
                    }
                }

                usersData.value = tempList
            }

        return usersData
    }

    fun createUser(idToken: String, user: User): MutableLiveData<Boolean> {
        val userCreated = MutableLiveData<Boolean>()

        db.collection(FirestoreCollections.USERS.value)
            .document(idToken)
            .set(user)
            .addOnSuccessListener {
                userCreated.value = true
            }
            .addOnFailureListener {
                userCreated.value = false
            }

        return userCreated
    }

    private fun uploadUserPicture(path: String, callback: (success: Boolean, imageURL: String?) -> Unit) {
        val userID = Auth.getCurrentUserID()
        val ref = storageRef.child("pictures/users/{$userID}.jpg")
        ref.putStream(FileInputStream(File(path)))
            .addOnSuccessListener {
                ref.downloadUrl
                    .addOnSuccessListener {
                        Log.d("upload", "successs");
                        callback(true, it.toString())
                    }
                    .addOnFailureListener {
                        Log.d("upload", "false");
                        callback(false, null)
                    }
            }
            .addOnFailureListener {
                callback(false, null)
            }
    }

    fun updateFCMToken(token: String, callback: (success: Boolean) -> Unit) {
        db.collection(FirestoreCollections.USERS.value)
            .document(Auth.getCurrentUserID())
            .update(mapOf("token" to token))
            .addOnSuccessListener {
                callback(true)
            }
            .addOnFailureListener {
                callback(false)
            }
    }
}