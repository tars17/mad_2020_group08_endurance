package com.mad2020.group8.endurance.helper

import com.google.firebase.auth.FirebaseAuth

object Auth {
    private val instance = FirebaseAuth.getInstance()

    fun getCurrentUserID(): String {
        if (instance.currentUser != null) {
            return instance.currentUser!!.uid
        }

        return ""
    }

    fun logout() {
        instance.signOut()
    }
}