import * as functions from 'firebase-functions';
import { getUsersByID } from './user';
import { prepareNotification, trySendNotificationToUser } from './notification';
import { getAdvertisementByID } from './advertisement';
import { intersection, xor } from 'lodash';

export const onInterestedUsersUpdate = functions.firestore
  .document('users_items_of_interests/{userID}')
  .onUpdate(async (change, context) => {
    // When a user is interested in buying an item on sale, 
    // the seller is notified.
    if (change.after.exists) {
      const previousSnap = change.before.data() || {};
      const currentSnap = change.after.data();

      const oldItemKeys = Object.keys(previousSnap);
      const currentItemKeys = Object.keys(currentSnap);
      const itemKeys = [];

      const keysInBoth = intersection(currentItemKeys, oldItemKeys);
      // all keys are new items
      if (keysInBoth.length === 0) {
        itemKeys.push(...currentItemKeys);
      } else {
        // only some keys are added
        itemKeys.push(...xor(keysInBoth, currentItemKeys));
      }

      itemKeys.forEach(async (itemKey: string) => {
        const advertisement = await getAdvertisementByID(itemKey);
        if (advertisement.exists) {
          const advertisementData = advertisement.data();
          const [currentUser, owner] = await getUsersByID([context.params.userID, advertisementData?.userId]);
          if (currentUser.exists && owner.exists) {
            const currentUserData = currentUser.data();
            
            await trySendNotificationToUser(
              owner,
              prepareNotification(
                `${advertisementData?.title}`,
                `${currentUserData?.fullname} is interested in buying ${advertisementData?.title}`,
                {
                  itemId: itemKey,
                  title: `${advertisementData?.title}`,
                  body: `${currentUserData?.fullname} is interested in buying ${advertisementData?.title}`,
                }
              )
            );
          }
        }
      });
    }

    return true;
  });
