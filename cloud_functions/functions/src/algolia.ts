import algolia from 'algoliasearch';
import * as admin from 'firebase-admin';

const client = algolia("JXINXIP2GS", "b69a7f314e5af9aa2d1a00eae172b9fe");
const itemIndex = client.initIndex("ITEMS");

const indexExistingItems = async () => {
  try {
    const items = await admin.firestore().collection('advertisements').get();
    if (!items.empty) {
      const indexPromises: any = [];
      items.docs.forEach((doc) => {
        const data = doc.data();
        if (data) {
          indexPromises.push(
            itemIndex.saveObject({
              ...data,
              objectID: data.id
            })
          )
        }
      })

      await Promise.all(indexPromises);
    }
  } catch(err) {
    console.log("Error while trying to index existing items");
    console.log(err);
  }
};

export {
  client,
  itemIndex,
  indexExistingItems
}
