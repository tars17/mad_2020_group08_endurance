import * as functions from 'firebase-functions';
import { itemIndex } from './algolia';

export const onItemDelete = functions.firestore
  .document('advertisements/{adID}')
  .onDelete(async (snap, context) => {
    // remove index to algolia
    await itemIndex.deleteObject(context.params.adID);
    return true;
  });
