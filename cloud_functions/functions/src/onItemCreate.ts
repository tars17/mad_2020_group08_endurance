import * as functions from 'firebase-functions';
import { itemIndex } from './algolia';

export const onItemCreate = functions.firestore
  .document('advertisements/{adID}')
  .onCreate(async (snap, context) => {
    // add index to algolia
    await itemIndex.saveObject({ 
      ...snap.data(),
      objectID: context.params.adID
    });

    return true;
  });
