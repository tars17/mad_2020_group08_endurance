import * as admin from 'firebase-admin';

export function getUserByID(userID: string) {
  return admin.firestore().collection('users').doc(userID).get();
}

// TODO: improve this to use whereIn query
export function getUsersByID(userIDs: string[]) {
  const userPromises: Promise<FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData>>[] = [];
  userIDs.forEach((userID) => userPromises.push(getUserByID(userID)));
  return Promise.all(userPromises);
}