import * as admin from 'firebase-admin';
import { getUserByID } from './user';

export function sendNotification(toUser: string, notification: admin.messaging.MessagingPayload) {
  return admin.messaging().sendToDevice(toUser, notification);
}

export async function trySendNotificationToUser(
  user: string | FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData>,
  notification: admin.messaging.MessagingPayload
) {
  let userSnap: FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData>|null = null;
  if (typeof user === 'object') {
    userSnap = user;
  } else {
    userSnap = await getUserByID(user);
  }

  console.log("Trying to send notification to user");
  console.log(userSnap);
  if (userSnap.exists) {
    const userData = userSnap.data();
    if (userData?.token) {
      await sendNotification(userData.token, notification);
    }

    console.log("Notification sent to user");
  }
}

export function prepareNotification(title: string, body: string, data?: admin.messaging.DataMessagePayload) {
  return {
    notification: {
      title,
      body
    },
    data: data || {}
  }
}
