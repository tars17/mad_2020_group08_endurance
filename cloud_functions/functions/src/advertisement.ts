import * as admin from 'firebase-admin';

export function getAdvertisementByID(adID: string) {
  return admin.firestore().collection('advertisements').doc(adID).get();
}
