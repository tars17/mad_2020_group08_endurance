import * as functions from 'firebase-functions';
import { getUsersByID } from './user';
import { prepareNotification, trySendNotificationToUser } from './notification';
import { itemIndex } from './algolia';

export const onItemUpdate = functions.firestore
  .document('advertisements/{adID}')
  .onUpdate(async (change, context) => {
    // When a seller sets an item as no more on sale, 
    // all the users interested in that object are notified.
    const dataBefore = change.before.data();
    const dataAfter = change.after.data();
    if (!dataBefore["blocked"] && dataAfter["blocked"]) {
      if (dataAfter.likedPeopleList) {
        const users = await getUsersByID(dataAfter.likedPeopleList);
        const notification = prepareNotification(
          `${dataAfter.title}`,
          `The item ${dataAfter.title} that you are interested in is removed from sale.`,
          {
            itemId: context.params.adID,
            title: `${dataAfter.title}`,
            body: `The item ${dataAfter.title} that you are interested in is removed from sale.`,
          }
        );

        const notificationPromises: any = [];
        users.forEach((user) => {
          notificationPromises.push(trySendNotificationToUser(user, notification));
        });

        await Promise.all(notificationPromises);
      }
    }

    // When a seller sets an item as sold, the buyer and all the other users that were interested 
    // in that object are notified of the event.
    if (!dataBefore["sold"] && dataAfter["sold"]) {
      // send notification to the buyer
      await trySendNotificationToUser(dataAfter.soldToUserId, prepareNotification(
        `You bought ${dataAfter['title']}`,
        `The owner of item ${dataAfter['title']} has sold this to you.`,
        {
          itemId: context.params.adID,
          title: `You bought ${dataAfter['title']}`,
          body: `The owner of item ${dataAfter['title']} has sold this to you.`,
        }
      ));

      // notify all interested users that the item is no more for sale
      if (dataAfter.likedPeopleList) {
        const users = await getUsersByID(dataAfter.likedPeopleList);
        const notification = prepareNotification(
          `${dataAfter.title}`,
          `The item ${dataAfter.title} that you are interested in is removed from sale.`,
          {
            itemId: context.params.adID,
            title: `${dataAfter.title}`,
            body: `The item ${dataAfter.title} that you are interested in is removed from sale.`,
          }
        );

        const notificationPromises: any = [];
        users.forEach((user) => {
          notificationPromises.push(trySendNotificationToUser(user, notification));
        });

        await Promise.all(notificationPromises);
      }
    }

    // update index in algolia
    await itemIndex.partialUpdateObject(
      { ...dataAfter, objectID: dataAfter.id }, 
      { createIfNotExists: true }
    );

    return true;
  });
