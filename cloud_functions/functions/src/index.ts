import { onInterestedUsersUpdate } from './onInterestedUsersUpdate';
import * as admin from 'firebase-admin';
import { onItemUpdate } from './onItemUpdate';
import { onItemCreate } from './onItemCreate';
import { onItemDelete } from './onItemDelete';

admin.initializeApp();

exports.onItemUpdate = onItemUpdate;
exports.onItemCreate = onItemCreate;
exports.onItemDelete = onItemDelete;
exports.onInterestedUsersUpdate = onInterestedUsersUpdate;
